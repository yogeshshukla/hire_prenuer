<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class SellerAddress extends Model
{
	public $table='seller_address';
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'house_no', 'locality', 'zip','city', 'state', 'country'   
    ];

    public static function sellerAddress($request,$user_id){
    		$data = $request;
			$user = new SellerAddress();
			$user->user_id = $user_id;
			$user->house_no = $data['house_no'];
			$user->locality = $data['locality'];
			$user->zip = $data['zip'];
			$user->city = $data['city'];
			$user->state = $data['state'];
			$user->country = $data['country'];
			$user->save();
			
	}

}
