<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SellerOffers extends Model
{
	public $table='seller_offer';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'project_name', 'project_description', 'includes','excludes','notes','price', 'discount_price'   
    ];

    public static function sellerOffers($data,$user_id){
    		
			$user = new SellerOffers();
			$user->user_id = $user_id;
			$user->project_name = $data['project_name'];
			$user->project_description = $data['project_description'];
			$user->includes = $data['includes'];
			$user->excludes = $data['excludes'];
			$user->notes = $data['notes'];
			$user->price = $data['price'];
			$user->discount_price = $data['discount_price'];
			$user->save();
			
	}
}
