<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Illuminate\Support\Facades\Mail;
use App\Emails;
use Auth;



class Emails extends Model
{
    

    public $table='emails';

    /**
     * The aibutes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_email', 'receiver_email', 'subject', 'body'
    ];

    /**
    *send email
    ***/
    public function createEmail(){
    	$data = Input::all ();

        $image = Input::file ( 'attachment' );
        $img = time () . '.' . $image->getClientOriginalExtension ();
        $destinationPath = public_path ('assets/img/emailAttachment' );
      //  File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
        $image->move ( $destinationPath, $img );
        $email = new Emails ();
        $email->receiver_email = Input::get ('receiver');
        $email->sender_email = Auth::user()->email;
        $email->subject = Input::get ('subject' );
        $email->body = Input::get ('message' );
        $email->save ();
        

        $email_attachment = new EmailAttachment ();
        $email_attachment->attachment = $img;
        $email_attachment->email_id = $email->id;
        $email_attachment->save ();
        

        return $email;
    }
}
