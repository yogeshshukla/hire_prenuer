<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SellerOffers;
use App\SellerAddress;
class ProfileController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

   
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
   // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getSellerProfile','sellerProfile','updateSellerProfile']]);
    }

    
	protected function sellerProfile($user_id=null)
    {
    	//echo Auth::user()->id;die;
    	if($user_id == null){
    	$user_id = Auth::user()->id;

    	}
    //echo $user_id;
		//$seller = User::find($user_id)->sellerDetails;
        $user = User::getUserDetail($user_id);
      //  print_r($user);die;
		return view('seller_profile', ['data' => $user]);
	}

	protected function getSellerProfile($user_id=false)
    {
    	if($user_id ==false){
    		$user_id = Auth::user()->id;
    	}
		$user = User::getUserDetail($user_id);
		//print_r($user);
		/*if(Auth::user()->role=='Seller'){
			$sellerParam = 'Seller';
		}else {
			if($sellerParam=='seller' && Auth::user()->role=='User'){
				$sellerParam = 'Seller';
			}
		}	
		*/
		return view('update_seller_profile', ['data' => $user, 'seller'=>'Seller','user_id'=>$user_id]);
	}


	protected function updateSellerProfile(Request $request)
    {
		  $validation = Validator::make($request->all(), [
            'marketing_summary' => 'max:500',
			'skills' => 'max:150',
            'industry' => 'max:100',
            'experience' => 'max:350',
			'career_degree' => 'max:100',
            'awards' => 'max:500',
        ]);
		if ( $validation->fails() ) {
			// change below as required
			return \Redirect::back()->withInput()->withErrors( $validation->messages() );
		}
		$data = $request->all();
		
		if($data['user_id']!=""){
            $user_id = $data['user_id'];
        }else{
            $user_id = Auth::user()->id;
        }
       // print_r($data);die;
		$user = User::updateSeller($data);
		if($user){
			if(!empty(Auth::user()->id)){
				return redirect('profile')->with('profile_updated', 'Profile updated Successfully! ');
			}else{
			return redirect('auth/seller_offer/'.$user_id);
			}
		}
		
		$seller = User::find(Auth::user()->id)->sellerDetails;
		return view('update_seller_profile', ['data' => $seller]);
	}
}
