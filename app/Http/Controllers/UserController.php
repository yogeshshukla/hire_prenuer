<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\User;
class UserController extends Controller
{
   public static function getUser(){
   	$user = new User();
   	return $data = $user->getUserData();
   }	

   public function getSearch(){

   }

   public static function getUserbySkill($skill){
   	 return	$data = User::getUserbySkill($skill);
   }

   public function searchProvider(Request $request){
   		$skill = $request->get('skills');
   		$data = User::getUserbySearch($request);
   		return view('searchResult',array('skill'=>$skill,'data'=>$data));
   }


}
