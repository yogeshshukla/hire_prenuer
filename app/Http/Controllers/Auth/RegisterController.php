<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use App\SellerAddress;
use App\SellerOffers;
use App\SellerDetails;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['sellerAddress','sellerAddressData','sellerOffer','sellerOfferData','sellerAgreement','sellerAgreementData','updateRole']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	protected function showSellerRegistrationForm($seller=false)
    {
		return view('auth.register_seller', ['seller'=>$seller]);
		
		
    }

	protected function registerSeller(Request $request)
    {
		//print_r($request->all());
		//exit;
         $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
			'title' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
		if ( $validation->fails() ) {
			// change below as required
			return \Redirect::back()->withInput()->withErrors( $validation->messages() );
		}
		$data = $request->all();
		$user = User::createSeller($data);
        //print_r($user);die;
		if($user){
            if($request->get('seller') == 'seller'){
			return redirect('auth/seller_adddress/'.$user)->with('account_created', 'Account Created Successfully! Please login');
            }else{
                return redirect('auth/login')->with('account_created', 'Account Created Successfully! Please login');
            }
		}
		
    }


    public function updateRole($user_id = ""){
          $user = User::updateRole($user_id);
          return redirect('auth/seller_adddress/'.$user_id); 
    }

    public function sellerAddress($user_id = ""){ 

        return view('registration_two',array('user_id' => $user_id ));
    }

    public function sellerAddressData(Request $request){

        $validation = Validator::make($request->all(), [
            'house_no' => 'required|max:255',
            'locality' => 'required|max:255',
            'zip' => 'required|numeric',
            'city' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'country' => 'required|string|max:255',
        ]);
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
       
        //dd(Input::all());
        $user_id = $request->get('user_id');
        if(isset(Auth::user()->id)){
            $user_id = Auth::user()->id;
        }

         $seller_address = SellerAddress::sellerAddress($request->all(),$user_id);   
         return redirect('update-profile/'.$user_id);
        }


    public function sellerOffer($user_id = ""){
        return view('seller_offer',array('user_id' =>$user_id));
    }


    public function sellerOfferData(Request $request){
        $validation = Validator::make($request->all(), [
            'project_name' => 'required|max:255',
            'project_description' => 'required|max:400',
            'includes' => 'required|min:96',
            'excludes' => 'required|min:96',
            'notes' => 'required|max:300',
            'price' => 'required|numeric',
            'discount_price' => 'required|numeric',
        ]);
        if ( $validation->fails() ) {
            // change below as required
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $user_id = $request->get('user_id');
        if(isset(Auth::user()->id)){
            $user_id = Auth::user()->id;
        }
        $seller_offers = SellerOffers::sellerOffers($request->all(),$user_id); 
         return redirect('auth/seller_agrrement/'.$user_id);
    }

    public function sellerAgreement($user_id = ""){
        return view('seller_agreement',array('user_id' =>$user_id));
    }

    public function sellerAgreementData(Request $request){
        $data = $request->all();
        if($data['agree'] == 0){
        
        $sellerOffer = SellerOffers::where('user_id',$data['user_id']);
        $sellerOffer->delete();
        
        $sellerDetails = SellerDetails::where('user_id',$data['user_id']);
        $sellerDetails->delete();

        $sellerAddress = SellerAddress::where('user_id',$data['user_id']);
        $sellerAddress->delete();

        $user = User::find($data['user_id'])->update(['role' => 'User']);
        
        return redirect('auth/register/seller');
        }else{
              return redirect('auth/login');
        }
    }

	protected function sellerProfile()
    {
        return view('seller_profile');
		
		
    }
}
