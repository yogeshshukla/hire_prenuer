<?php

namespace App\Http\Controllers;

use App\Emails;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use DB;
use App\User;

class EmailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view("/conversation/create");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Input::all ();
		$rules = array (
				'receiver' => 'required',
				'subject' => 'required',
				'userfile' => 'mimes:jpg,png,gif,jpeg,JPG,GIF,PNG,JPEG'
		);
		$validator = Validator::make ( $data, $rules );
		
		if ($validator->fails ()) {
			return Redirect::to ( '/admin/add-product' )->withInput ( Input::except ( 'status' ) )->withErrors ( $validator );
		} else { 
			$product = new Emails();
			$return = $product->createEmail();
			if ($return) {
				return Redirect::to ( '/home' )->with ( "confirm", "You have successfully sent message! " );
			}
		}
    }


    //show sent emails
    public function sent_emails(){

        $sent_emails = Emails::where('sender_email','=',Auth::user()->email)->orderBy('id','desc')->get();
        $sent_email = DB::table('emails')->select('emails.sender_email','emails.receiver_email','emails.subject','emails.body','email_attachment.attachment')
                    ->leftJoin('email_attachment', 'emails.id', 'email_attachment.email_id')
                    ->where('sender_email',Auth::user()->email)
                    ->orderBy('emails.id','DESC');
        return view("/conversation/sent_emails" , ['data'=>$sent_emails]);
    }

    public function received_emails(){

        $sent_emails = Emails::where('receiver_email','=',Auth::user()->email)->orderBy('id','desc')->get();
        $sent_email = DB::table('emails')->select('emails.sender_email','emails.receiver_email','emails.subject','emails.body','email_attachment.attachment')
                    ->leftJoin('email_attachment', 'emails.id', 'email_attachment.email_id')
                    ->where('sender_email',Auth::user()->email)
                    ->orderBy('emails.id','DESC');
        return view("/conversation/received_emails" , ['data'=>$sent_emails]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emails  $emails
     * @return \Illuminate\Http\Response
     */
    public function show(Emails $emails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emails  $emails
     * @return \Illuminate\Http\Response
     */
    public function edit(Emails $emails)
    {
        //
    }
    public function configuration(){
        $files = glob('/*'); // get all file names
        print_r($files);
        foreach($files as $file){ // iterate files
         // if(is_file($file))
           // unlink($file); // delete file
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emails  $emails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Emails $emails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emails  $emails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emails $emails)
    {
        //
    }
}
