<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use DB;
use App\Service;
USE App\ProductCategory;

class ServiceController extends Controller
{
	
	public function addService()
    {
		$data = Input::all ();
		$rules = array (
				'cat_id' => 'required',
				'service' => 'required'
		)
		;
		$validator = Validator::make ( $data, $rules );
		
		if ($validator->fails ()) {
			return Redirect::to ( '/admin/add-service' )->withInput ( Input::except ( 'status' ) )->withErrors ( $validator );
		} else {
			$service = new Service();
			$return = $service->addService ();
			if ($return) {
				return Redirect::to ( '/admin/services' )->with ( "confirm", "You have successfully Added a service category! " );
			}
		}
    }    

	public function viewAddService(){

		//for stats
		$serviceCat = ProductCategory::where('status',1)->orderBy('id','desc')->get();
		$ServiceEnableTotal = Service::where('status',1)->get();
    	$ServiceDisableTotal = Service::where('status',0)->get();
    	//end stats

    	return view("/admin/add-service" , ['data'=>$serviceCat, 'ServiceEnableTotal'=>$ServiceEnableTotal, 'ServiceDisableTotal'=>$ServiceDisableTotal]);

		//return view('admin.add-product-category');
	}


	public function getServices()
    {	
    	//for stats
		$ServiceTotal = Service::orderBy('id','desc')->get();
    	$ServiceEnableTotal = Service::where('status',1)->get();
    	$ServiceDisableTotal = Service::where('status',0)->get();
    	return view("/admin/services" , ['data'=>$ServiceTotal, 'ServiceEnableTotal'=>$ServiceEnableTotal, 'ServiceDisableTotal'=>$ServiceDisableTotal]);
    	//end stats 
    }

    public function updateService($id)
    {
    	if($id){
    		
    		//for stats
    		$ServiceEnableTotal = Service::where('status',1)->get();
    		$ServiceDisableTotal = Service::where('status',0)->get();
    		//end stats

    	$detail = Service::where('id',$id)->first();
		    if($detail){
		    	return view('/admin/update-service', ['data'=>$detail, 'ServiceEnableTotal'=>$ServiceEnableTotal, 'ServiceDisableTotal'=>$ServiceDisableTotal]);
		    }else{
		    	return Redirect::to ( '/admin/update-service');
		    }
        }else{
        	return Redirect::to ( '/admin/update-service');
        }
    }

    public static function updateServiceData()
    {
		$data = Input::all ();
		$rules = array (
				'service' => 'required'
		);
		$validator = Validator::make ( $data, $rules );
		
		if ($validator->fails ()) {
			return Redirect::to ( '/admin/update-service' )->withInput ( Input::except ( 'status' ) )->withErrors ( $validator );
		} else {
			//print_r($data);die;
			$pc = Service::updateServiceData();
			
			if ($pc) {
				return Redirect::to ( '/admin/services' )->with ( "confirm", "You have successfully Updated Product Category! " );
			}else{
				return Redirect::to ( '/admin/services' )->with ( "error", "Some Error Occured While Updating Service! " );
			}

		}
    } 


    public static function deleteService($id)
    {
    	if($id){ 
	    	$user = Service::find($id);    
			$user->delete();
		    if($user){
		    	return Redirect::to('/admin/product-categories')->with("confirm","Product Categories Deleted Successfully");
		    }else{
		    	return Redirect::to ( '/admin/services');
		    }
        }else{
        	return Redirect::to ( '/admin/services');
        }
    }

    public static function enableDisableService($id)
    {
    	if($id){ 
	    	$pc = Service::find($id);    
			if($pc->status == 1){
				$status = 0;
				$userInactive = Service::enableDisableService($id,$status);
				return Redirect::to('/admin/services')->with("confirm","Services Inactive Successfully");
			}else{
				$status = 1;
				$userInactive = Service::enableDisableService($id,$status);
				return Redirect::to('/admin/services')->with("confirm","Services Active Successfully");
			}
        }else{ 
        	return Redirect::to ( '/admin/services');
        }
    }

	public function getCategory(){
			$service = new Service();
			$data = $service->getCategory();
			return view('service',array('data' =>$data ));
	}

	public static function getService($cat_id=""){
		$service = new Service();
		$data = $service->getService($cat_id);
		return $data;
	}	    
	
}
