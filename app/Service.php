<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Hash;
use Redirect;
use Illuminate\Support\Facades\Mail;
use App\Service;
use App\ProductCategory;
//use App\DB;
use Illuminate\Support\Facades\DB;
class Service extends Model
{

    public $table='services';

    /**
     * The aibutes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cat_id','service','description','image','status',
    ];

    //add product category
    public function addService()
    {
      
        $image = Input::file ( 'image' );
        $img = time () . '.' . $image->getClientOriginalExtension ();
        $destinationPath = public_path ('assets/img/service' );
        $image->move ( $destinationPath, $img );
        
        $data = Input::all ();
        $service = new Service ();
        $service->cat_id = Input::get ('cat_id');
        $service->service = Input::get ('service');
        $service->description = Input::get ('description');
        $service->image = $img;
        $service->status = Input::get ('status');
        $service->save ();
        return $service;
    }


 //Update Service
    public static function updateServiceData()
    {

        $data = Input::all ();

       /* if(Input::get('status') == 1){
            $status = 1;
        }else{
            $status = 0;
        }
echo $status;die;
*/        $data = Service::where('id', Input::get('id'))
           ->update ( [
            'service' => Input::get('service'),
            'updated_at'=> date("Y-m-d H:i:s")
           ] );
        
        return $data;
    }

//enable Disabled Service
    public static function enableDisableService($id,$status)
    {
        $data = Service::where('id', $id)
           ->update ( [
             'status' => $status,
             'updated_at'=> date("Y-m-d H:i:s")
           ] );
        return $data;
    }

    
    //get services with category
    public function getCategory(){
            $productCat = ProductCategory::where('status',1)->get();
          /*  //dd($productCat);
            $service = [];
          $productCat = DB::table('product_categories')
                ->join('services', 'product_categories.id', '=', 'services.cat_id')
                ->where('product_categories.status',1)
                ->where('services.status',1)
                ->get();*/
            return $productCat;

    }

    public function getService($cat_id = ""){
      if($cat_id=="")
        $service = Service::where('status',1)->get();
      else
        $service = Service::where('cat_id',$cat_id)->get();
        return $service;
    }


}
