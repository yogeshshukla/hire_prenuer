<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerDetails extends Model
{
	public $table='seller_details';
	public function seller()
    {
        return $this->belongsTo('App\User');
    }
    
}
