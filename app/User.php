<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Support\Facades\Hash;
use Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use App\SellerDetails;


class User extends Authenticatable
{
    use Notifiable;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'email', 'password','role', 'mobile', 'website', 'address', 'provider', 'provider_id'   
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
   
	public static function createSeller($data){
		$user = new User();
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->title = isset($data['title']) ? $data['title'] : null;
		$user->password = bcrypt($data['password']);
		$user->status = 'Inactive';
		$user->role= isset($data['seller']) && !empty($data['seller']) ? 'Seller' : 'User';
		$user->save();
		if($user->id){
		
			SellerDetails::insert(['user_id' => $user->id]);
		}
		return $user->id;
	}
	
	public static function updateRole($user_id=""){
			$user = User::find($user_id)->update(['role' => 'Seller']);
			return true;
	}

	public static function updateSeller($data){
		if($data['user_id']!=""){
            $user_id = $data['user_id'];
        }else{
            $user_id = Auth::user()->id;
        }
        $user = User::find($user_id);
		if($user){
			if(isset($data['name'])){
            $user->name = $data['name'];
			}
            //$user->email = $data['email'];
			if(isset($data['title'])){
            $user->title = $data['title'];
			}
            
            if(isset($data['mobile'])){
            $user->mobile = $data['mobile'];
			}
            if(isset($data['website'])){
            $user->website = $data['website'];
			}
            if(isset($data['address'])){
            $user->address = $data['address'];
            }
			if(isset($data['seller']) && !empty($data['seller'])){
				$user->role = 'Seller';
			}
			
			if(isset($data['profile_pic']) && !empty($data['profile_pic']) ){
				$storage_path = 'profile/';
				$name = $data['profile_pic']->getClientOriginalName();
					
				//$extension = $data['profile_pic']->getClientOriginalExtension();
				$file_name = $name;
				
				$file_upload =  Storage::disk('local')->put($storage_path.$file_name, file_get_contents($data['profile_pic']));
				if($file_upload){
					$user->image = $storage_path.$file_name;
				}
			}
			$user->save();
			if($user){
				$sellerDetails = SellerDetails::where('user_id', $user_id)->first();
				if($sellerDetails){
					$sellerDetails->about_me = isset($data['marketing_summary'])?$data['marketing_summary']:null;
					$sellerDetails->experience = isset($data['marketing_summary'])?$data['experience']:null;
					$sellerDetails->skills = isset($data['marketing_summary'])?$data['skills']:null;
					$sellerDetails->industries = isset($data['marketing_summary'])?$data['industry']:null;
					$sellerDetails->degrees = isset($data['marketing_summary'])?$data['career_degree']:null;
					$sellerDetails->award_certification = isset($data['marketing_summary'])?$data['awards']:null;
					$sellerDetails->save();
				}
			}else {
				return false;
			}
		}else { // user not found
			die('user not found');
		}
		
		return true;
	}
	public function profilePhoto()
	{
		if(!empty(Auth::user()->image) ){
			if(file_exists( storage_path('app/'.Auth::user()->image)) ) {
				return  url('storage/app/'.Auth::user()->image);
			} else {
				return url('storage/app/profile/profile-placeholder.jpg');
			}
		}else {
			return url('storage/app/profile/profile-placeholder.jpg');
		}
		     
	}
	public function sellerDetails()
    {
        return $this->hasOne('App\SellerDetails');
    }

   public function registration()
   {
    $data = Input::all();
    //dd(   $data);
    $user = new User();
    $user->name = Input::get('val_username');
    $user->email = Input::get('val_email');
    $user->password = Hash::make(Input::get('val_password'));
    $user->status = 'Inactive';
    $user->role= Input::get('val_role');
    $user->remember_token= Input::get('_token');
    $user->save();

    Mail::send('users.mails.welcome_email',[
            'user' => $user,
            
    ],function ($message) use ($user){
        $message->from('ynandan55@gmail.com');
        $message->to ($user->email, $user->name)->subject('Welcome to Laravel Website system ');
    });
   // return Redirect::to('/createpayment');
    //return redirect('createpayment');
    return ;
   }

   public static function updateUserData($request)
    {
        if(Input::get('status') == 1){
            $status = 'Active';
        }else{
            $status = 'Inactive';
        }

        $data = User::where('id', Input::get('id'))
           ->update ( [
             'name' => Input::get('val_username'),
              'role' => Input::get('val_role'),
             'status' => $status,
             'updated_at'=> date("Y-m-d H:i:s")
           ] );

           if($data){
            return true;
           }else{
            return false;
           }
    }

    public static function enableDisableUser($id,$status)
    {
        $data = User::where('id', $id)
           ->update ( [
             'status' => $status,
             'updated_at'=> date("Y-m-d H:i:s")
           ] );
        return $data;
    }
	
     public static function updateProfileData()
    {
       
       $id = Auth::user()->id;
       $imageuser = Auth::user()->image;

       if(Input::file ( 'userfile' )){

        $image = Input::file ( 'userfile' );
        $img = time () . '.' . $image->getClientOriginalExtension ();
        $destinationPath = public_path ('assets/img/users' );
        $image->move ( $destinationPath, $img );

        if($imageuser){
            $filename = public_path().'assets/img/users/'.$imageuser;
            @unlink($filename);
        }

          $data = User::where('id', $id)
           ->update ( [
            'name' => Input::get('val_username'),
            'image' => $img,
            'updated_at'=> date("Y-m-d H:i:s")
           ] );

        }else{

        $data = User::where('id', $id)
           ->update ( [
             'name' => Input::get('val_username'),
             'updated_at'=> date("Y-m-d H:i:s")
           ] );
        }
    return $data;
           
    }

    public  function getUserData(){
    	
    	$data =  DB::table('users')
            ->join('seller_details', 'users.id', '=', 'seller_details.user_id')
            ->join('seller_offer', 'users.id', '=', 'seller_offer.user_id')
            ->where(array('role'=>'Seller'))
            ->select('users.id','users.name','users.image','users.title','seller_offer.price')
            ->limit(2)
            ->get();
	return $data;
    }

      public static function getUserDetail($user_id = null){
        
        $data =  DB::table('users')
            ->leftJoin('seller_details', 'users.id', '=', 'seller_details.user_id')
            ->leftJoin('seller_offer', 'users.id', '=', 'seller_offer.user_id')
            ->where(array('users.id'=>$user_id))
            ->select('users.id','users.name','users.role','users.email','users.title','mobile','website','seller_details.*','seller_offer.*', 'seller_offer.id as offer_id', 'users.image','users.title','seller_offer.price')
            ->first();
           // print_r($data);
    return $data;
    }

    public static function getUserbySearch($request){
        $skill = $request->get('skills');
        $data =  DB::table('users')
            ->leftJoin('seller_details', 'users.id', '=', 'seller_details.user_id')
            ->leftJoin('seller_offer', 'users.id', '=', 'seller_offer.user_id')
            ->where(array('role'=>'Seller'))
            ->where('skills', 'like', $skill. '%')
            ->select('users.id','users.name','mobile','website','seller_details.*','users.image','users.title','seller_offer.price')
            ->get();
            //print_r($data);die;
    return $data;
    }

     public static function getUserbySkill($skill){
        $data =  DB::table('users')
            ->leftJoin('seller_details', 'users.id', '=', 'seller_details.user_id')
            ->leftJoin('seller_offer', 'users.id', '=', 'seller_offer.user_id')
            ->where(array('role'=>'Seller'))
            ->where('skills', 'like', $skill. '%')
            ->select('users.id','users.name','mobile','website','seller_details.*','users.image','users.title','seller_offer.price')
            ->limit(4)
            ->get();
            //print_r($data);die;
    return $data;
    }

}
