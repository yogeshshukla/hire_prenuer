-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 18, 2018 at 11:27 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hire_preneur`
--

-- --------------------------------------------------------

--
-- Table structure for table `applied_candidate`
--

CREATE TABLE `applied_candidate` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coverletter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipdata` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `comments` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `sender_email`, `receiver_email`, `subject`, `body`, `is_read`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'anil1@gmail.com', 'test@gmail.com', 'testing', 'testing message', 0, 0, '2018-01-03 15:09:10', '2018-01-03 15:09:10'),
(2, 'anil1@gmail.com', 'test@gmail.com', 'testing', 'testing message', 0, 0, '2018-01-03 15:10:54', '2018-01-03 15:10:54'),
(3, 'anil1@gmail.com', 'test@gmail.com', 'testing', 'testing message', 0, 0, '2018-01-03 15:12:28', '2018-01-03 15:12:28'),
(4, 'anil@gmail.com', 'anil1@gmail.com', 'test1', 'test1', 0, 0, '2018-01-08 17:47:42', '2018-01-08 17:47:42'),
(5, 'anil@gmail.com', 'anil1@gmail.com', 'test2', 'test2', 0, 0, '2018-01-08 17:49:46', '2018-01-08 17:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `email_attachment`
--

CREATE TABLE `email_attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_id` int(11) NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_attachment`
--

INSERT INTO `email_attachment` (`id`, `email_id`, `attachment`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 2, '1514963454.jpg', 0, '2018-01-03 15:10:54', '2018-01-03 15:10:54'),
(2, 3, '1514963548.jpg', 0, '2018-01-03 15:12:29', '2018-01-03 15:12:29'),
(3, 4, '1515404861.jpg', 0, '2018-01-08 17:47:44', '2018-01-08 17:47:44'),
(4, 5, '1515404986.jpg', 0, '2018-01-08 17:49:46', '2018-01-08 17:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, 'How to register?', '<p>Register directlysss...</p>', '1', '2017-09-06 20:18:20', '2017-09-06 14:48:20');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_08_01_130814_applied_candidate', 1),
(2, '2017_08_01_151844_users', 1),
(3, '2017_08_01_151917_password_resets', 1),
(4, '2017_08_02_144847_comments', 1),
(5, '2017_08_02_192246_ratings', 1),
(6, '2017_08_03_161729_add_ip_applied_candidate', 1),
(7, '2017_08_03_171045_reviews', 1),
(8, '2017_09_05_182542_posts', 1),
(9, '2017_09_05_182614_teams', 1),
(10, '2017_09_05_182633_testimonial', 1),
(11, '2017_09_06_194240_faqs', 1),
(12, '2017_09_08_173112_seos', 1),
(13, '2017_09_11_174954_post_comments', 1),
(14, '2017_09_11_215703_galleries', 1),
(15, '2017_09_12_200434_siteconfiguration', 1),
(16, '2017_09_12_213943_sociallinks', 1),
(17, '2017_09_13_174128_sliders', 1),
(18, '2017_09_14_141317_product_categories', 1),
(19, '2017_09_14_141326_products', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('shuklayogesh065@gmail.com', '$2y$10$HYwpLTSUkC7vAAcJ0TNVnueyYGldlGQd3AnuTxsALDoRHtu6P8102', '2017-12-30 14:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `author`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'aaaa', '1', '<p>aaaaa</p>', '1504647300.png', 'Active', '2017-09-05 16:05:00', '2017-09-05 16:34:13'),
(3, 'aaasasas', '1', 'aasasas', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `cat_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Television', '1', '2017-09-15 17:31:59', '2017-09-15 12:01:59'),
(2, 'Mobile', '0', '2017-09-15 17:32:23', '2017-09-15 12:02:23'),
(4, 'Refrigirator', '1', '2017-09-15 12:03:49', '2017-09-15 12:03:49'),
(5, 'Entertainment', '1', '2018-01-15 00:41:02', '2018-01-15 00:41:02'),
(6, 'IT/Software', '1', '2018-01-15 01:47:28', '2018-01-15 01:47:28');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `candidate_id` int(10) UNSIGNED NOT NULL,
  `ratings` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `candidate_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seller_address`
--

CREATE TABLE `seller_address` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `house_no` varchar(255) NOT NULL,
  `locality` varchar(355) NOT NULL,
  `zip` bigint(20) NOT NULL,
  `city` varchar(300) NOT NULL,
  `state` varchar(300) NOT NULL,
  `country` varchar(300) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_address`
--

INSERT INTO `seller_address` (`id`, `user_id`, `house_no`, `locality`, `zip`, `city`, `state`, `country`, `status`, `created_at`, `updated_at`) VALUES
(1, 25, '534', 'fgfdgfd', 4343453, 'dfgdfgd', 'dfgdfgfd', 'terterter', 1, '2018-01-18 16:54:33', '2018-01-18 11:24:33'),
(3, 27, '23', 'NAVEEN NAGAR', 232123, 'Bhopal', 'MP', 'India', 1, '2018-01-18 20:53:18', '2018-01-18 15:23:18'),
(4, 30, '12', 'ssfds', 1212121, 'sdfsdfs', 'sdfsdf', 'fsdfsdfs', 1, '2018-01-18 21:26:00', '2018-01-18 15:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `seller_details`
--

CREATE TABLE `seller_details` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `about_me` text,
  `experience` text,
  `skills` text,
  `industries` text,
  `degrees` varchar(255) DEFAULT NULL,
  `award_certification` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_details`
--

INSERT INTO `seller_details` (`id`, `user_id`, `about_me`, `experience`, `skills`, `industries`, `degrees`, `award_certification`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, NULL, NULL, NULL, NULL, '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 22, 'testing purpose summary', 'testing purpose experience', 'testing purpose skill', 'testing purpose industry', 'testing purpose degree', 'testing purpose award', '2018-01-13 22:06:18', '2018-01-14 06:21:00'),
(4, 23, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-15 01:15:31', '2018-01-15 01:15:31'),
(5, 24, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-16 01:24:46', '2018-01-16 01:24:46'),
(6, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-18 20:44:44', '2018-01-18 20:44:44'),
(7, 26, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-19 00:31:08', '2018-01-19 00:31:08'),
(8, 27, 'sfsdfsd', '10000', 'skills', 'IT', 'MCA', 'dsadsa', '2018-01-19 02:22:37', '2018-01-18 20:57:18'),
(9, 28, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-19 02:29:40', '2018-01-19 02:29:40'),
(10, 29, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-19 02:54:34', '2018-01-19 02:54:34'),
(11, 30, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-19 02:55:44', '2018-01-19 02:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `seller_offer`
--

CREATE TABLE `seller_offer` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `project_name` varchar(355) NOT NULL,
  `project_description` varchar(500) NOT NULL,
  `includes` varchar(355) NOT NULL,
  `excludes` varchar(355) NOT NULL,
  `price` varchar(200) NOT NULL,
  `discount_price` varchar(200) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_offer`
--

INSERT INTO `seller_offer` (`id`, `user_id`, `project_name`, `project_description`, `includes`, `excludes`, `price`, `discount_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 25, 'asdsfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', '34353', '345345', 1, '2018-01-18 18:58:07', '2018-01-18 13:28:07'),
(3, 27, 'Demo', 'Demo project', 'demo', 'nothing', '2345', '00', 1, '2018-01-18 20:53:55', '2018-01-18 15:23:55'),
(4, 30, 'dsadasd', 'fsdfsdfds', 'sdfsdfds', 'sdfsdfds', '32323', '2323', 1, '2018-01-18 21:26:18', '2018-01-18 15:56:18');

-- --------------------------------------------------------

--
-- Table structure for table `seos`
--

CREATE TABLE `seos` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seos`
--

INSERT INTO `seos` (`id`, `page_name`, `title`, `keywords`, `description`, `created_at`, `updated_at`) VALUES
(1, 'About', 'About Us', 'About Us', '<p>About Us</p>', '2017-09-11 13:31:55', '2017-09-11 08:01:55'),
(2, 'Term And Condition', 'Term & Condition', 'Term & Condition', '<p>Term &amp; Condition</p>', '2017-09-11 13:31:31', '2017-09-11 08:01:31'),
(3, 'Privacy policy', 'Privacy policy', 'Privacy policy', '<p>Privacy policy</p>', '2017-09-08 22:10:28', '2017-09-08 16:40:28'),
(4, 'Contact', 'contact us', 'contact us', '<p>contact us</p>', '2017-09-08 22:09:36', '2017-09-08 16:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `service` varchar(300) NOT NULL,
  `description` varchar(355) NOT NULL,
  `image` varchar(355) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = active,0=inactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `cat_id`, `service`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 'Design Art & Multimedia', 'testing', '1515951856.jpg', 1, '2018-01-15 01:44:17', '2018-01-15 01:44:17'),
(2, 6, 'Web Development & IT', 'Web Development & IT data', '1515952144.jpg', 1, '2018-01-15 01:49:04', '2018-01-15 02:03:56'),
(3, 6, 'Software Development', 'Software Development', '1515952181.jpg', 1, '2018-01-15 01:49:41', '2018-01-15 02:04:41'),
(4, 6, 'Web Development', 'Web Development', '1515952237.jpg', 1, '2018-01-15 01:50:37', '2018-01-15 01:50:37');

-- --------------------------------------------------------

--
-- Table structure for table `siteconfiguration`
--

CREATE TABLE `siteconfiguration` (
  `id` int(10) UNSIGNED NOT NULL,
  `website_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siteconfiguration`
--

INSERT INTO `siteconfiguration` (`id`, `website_name`, `website_title`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Laravel', 'A laravel site', 'logo.png', NULL, '2017-09-12 21:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'my first slider', 'my first slider my first slider my first slider my first slider my first slider my first slider', '1505337953.png', '1', '2017-09-13 21:25:53', '2017-09-13 21:35:18'),
(2, 'my second blog', 'my second blog my second blog my second blog my second blog my second blog my second blog my second blog', '1505338578.png', '1', '2017-09-13 21:36:18', '2017-09-13 21:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `sociallinks`
--

CREATE TABLE `sociallinks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sociallinks`
--

INSERT INTO `sociallinks` (`id`, `name`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Facebook', 'http://www.facebook.com', 1, '2017-09-13 04:12:12', '2017-09-13 13:08:32'),
(2, 'Twitter', 'http://www.twitter.com', 1, '2017-09-13 13:35:24', '2017-09-13 16:27:04'),
(3, 'Pintrest', 'http://www.pintrest.com', 1, '2017-09-13 15:52:00', '2017-09-13 16:26:57'),
(4, 'Instagram', 'http://www.instagram.com', 1, '2017-09-13 15:55:15', '2017-09-13 15:55:15');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `designation`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'team 1', 'team 1', '<p>team 1</p>', '1504722796.JPG', 'Inactive', '2017-09-06 13:03:16', '2017-09-06 13:17:49'),
(2, 'aa', 'aa', '<p>aaa</p>', '1504723571.JPG', 'Active', '2017-09-06 13:16:11', '2017-09-06 13:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Testimonial 11', '<p>Testimonialm11</p>', '1504720631.jpg', 'Inactive', '2017-09-06 12:27:11', '2017-09-06 12:33:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `title`, `email`, `provider`, `provider_id`, `mobile`, `website`, `address`, `password`, `status`, `role`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Yogesh Shukla', NULL, 'shuklayogesh065@gmail.com', '', '', '', '', '', '$2y$10$f/Wk8bM2nmq7TxcHGUc8M.qVgVE/bwaaGD2mEf4EbDQgTMTyuqkBe', 'Active', 'Admin', '1505319681.JPG', 's3BJMk4fuUrAJ3fVLiY8Vex6xSjnRrDFfhO3ZUQjRpvgiSZXapmSeFvwQkwh', '2017-09-05 18:30:00', '2017-09-15 13:39:01'),
(2, 'Astha Sharma', NULL, 'asthasharma1609@gmail.com', '', '', '', '', '', '$2y$10$byIwHawQHgsEGR7wFzVsKOQVXJgLgU/waQT2kafSeUUNqsBtytKIa', 'Active', 'User', '', 'l8AUHBEKxkbL3qyVhsf7cawNmYq63x6Dqf4UNTDW', '2017-09-11 10:56:36', '2017-09-11 10:57:05'),
(3, 'Ekart Logistic', NULL, 'ekart@gmail.com', '', '', '', '', '', '$2y$10$tYDuGpyKSOphv/ifmtcsb.wS.LaRluYSVXPUERZESaQtpriGqa4sm', 'Active', 'Seller', '', 'R6JphnOf3iHSx789ExXQkwyd5JIyEi2zx3HE8Lz4', '2017-09-15 13:42:56', '2017-09-15 13:43:22'),
(5, 'yogesh shukla', NULL, 'yogeshshukla@gmail.com', '', '', '', '', '', '$2y$10$XYIHkOwxuRMv0UCB4i5Vx.N1ldKQskHkNnYeXAxyc4bzslcUsJIo6', NULL, NULL, NULL, NULL, '2017-12-30 09:37:53', '2017-12-30 09:37:53'),
(6, 'yogesh shukla', 'pandit', 'shuklayogesh065@gmail.com', '', '', '', '', '', '123456', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:39:01', '2017-12-30 14:39:01'),
(7, 'yogesh shukla', 'pandit', 'shuklayogesh065@gmail.com', '', '', '', '', '', '$2y$10$SNlQ7iZzjHA4yF7nqMTXq.yCuazgbw77kouqstUGDl5xhvM1N4z8S', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:41:18', '2017-12-30 14:41:18'),
(8, 'yogesh shukla', 'pandit', 'yogeshshukl4a@gmail.com', '', '', '', '', '', '$2y$10$0TQ83rn4DTmD0eq..nF0ae3rnsBqCEzVLV/2abp.z41yzauunliLK', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:45:08', '2017-12-30 14:45:08'),
(9, 'yogesh shukla', 'pandit', 'shuklayogesh0645@gmail.com', '', '', '', '', '', '$2y$10$REVDzyG9Q6WPCxZJlZ0NKeD17O/ttjNlDDxHCV7IMDnIhAy0Nmhmu', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:45:54', '2017-12-30 14:45:54'),
(10, 'yogesh shukla', 'pandit', 'shuklayogesh078645@gmail.com', '', '', '', '', '', '$2y$10$Lh7.PXcbqtK29ERKILsGoeBloFUZtrbwJ9QIa9/RwOCgu5JOYVRhG', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:48:36', '2017-12-30 14:48:36'),
(11, 'yogesh shukla', 'pandit', 'shuklayogesh067895@gmail.com', '', '', '', '', '', '$2y$10$VTqFYvCoECvvGffYZ8C6P.xDUGy/trfIuJts97VrxoC.ebAc6Azci', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:52:10', '2017-12-30 14:52:10'),
(12, 'yogesh shukla', 'pandit', 'yogeshshhhhukla@gmail.com', '', '', '', '', '', '$2y$10$8y2rtYbqp59HX11hiKVI/e0QTDT8vtrbs6XRoNTNWYChvmYWaLBPK', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:53:19', '2017-12-30 14:53:19'),
(13, 'yogesh shukla', 'pandit', 'shuklayogesh0656@gmail.com', '', '', '', '', '', '$2y$10$WLlJF4D4BKGJXA3TZ8n4WeE4f0.u8K8ByWqnVxNODaCDfLIXx2/.m', 'Inactive', 'Seller', NULL, 'rBqNPKre7Lv0ICFBCUZ2hXlePkQQFBAyUxKUjsg7xgluy1DoeYhM8Kl0jrTE', '2017-12-30 14:56:47', '2017-12-30 14:56:47'),
(14, 'Anil', NULL, 'anil@gmail.com', '', '', NULL, NULL, NULL, '$2y$10$Cgfv7cSX5o/wqIIuDdwBROwVo/03HK26jrtP.WV4JAzL.je2Zg/bS', 'Active', 'Admin', NULL, '61u5netjTbz960CuiXsowdGn5rpANj848vhCJjxGZGz9eMR6bjYy2v8nZe40', '2018-01-02 14:23:14', '2018-01-02 14:23:14'),
(15, 'anil', NULL, 'anil1@gmail.com', '', '', NULL, NULL, NULL, '$2y$10$l9lsKNdJeUwt9siP1P9quOVZzeNAl83LN.M3MbaBvPevIPCu7h8ee', NULL, NULL, NULL, '721YXpnrdspjL4AlnsAA8SuLnoOdPFvG5cK4CARhtAdb209aLANTGTJSzgau', '2018-01-03 14:14:06', '2018-01-03 14:14:06'),
(16, 'Anil Gupta', 'Sr PHP Developer', 'anil_gupta73@ymail.com', 'facebook', '1663144147076793', '9300358503', 'google.com', 'indore', NULL, NULL, NULL, 'profile/Chrysanthemum.jpg', 'pX5bfK1tcvwP2mEF5XQrcsrlQLGtM3AXUc28AFOOjK1wGg0GxPi9w5Lh06n7', '2018-01-14 14:25:43', '2018-01-14 14:28:08'),
(17, 'अनिल गुप्ता ', NULL, NULL, 'twitter', '2534772607', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EXXz90MUDblC7WUUPJQwOZKyOSCdjIhiUtYKusCgFNDMnoksHG0WfIhdM8Tc', '2018-01-14 13:58:09', '2018-01-14 13:58:09'),
(18, 'Anant Gupta', 'Enterpreneur', 'anant@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$OHi9b/bYu0s3MAtF.eomMOfWEYcryA8F6oY/1IoOu6vAde4m8Wgse', 'Inactive', 'User', NULL, NULL, '2018-01-14 14:00:56', '2018-01-14 14:00:56'),
(19, 'Anant Gupta', 'Enterpreneur', 'anant1@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$FYNa5v1w1eOGz94l2jtQFutio7k/yGc1LkxKIndJZ5lffAjfn25XO', 'Inactive', 'User', NULL, NULL, '2018-01-14 14:02:10', '2018-01-14 14:02:10'),
(20, 'Anant Gupta', 'Enterpreneur', 'anant2@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$HsgyUXvALpfYchAUSP0xzu3EFibJ3OJ5wqsU7XzdUIlBWWhXs1cia', 'Inactive', 'User', NULL, NULL, '2018-01-14 14:03:23', '2018-01-14 14:03:23'),
(21, 'Anant Gupta', 'Enterpreneur', 'anant3@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$COuiQCknXjxiuPH3C.adCuz9y/JQN6oz76nnOLECrvpDntdn649m6', 'Inactive', 'User', NULL, NULL, '2018-01-14 14:04:51', '2018-01-14 14:04:51'),
(22, 'Anant Gupta', 'Enterpreneur', 'anant4@gmail.com', NULL, NULL, '9300358503', 'test,com', 'bhopal', '$2y$10$3EEm8lt9p1ZHV0ZJTaAihuKWyVRXDnTKTxiO2BgFg0dvPg4maGSyq', 'Inactive', 'Seller', NULL, 'PZmJSS0pLyRSTNj333nGjnihy1DYNdeUzYOVT1A1VqkhkDnUTXucHGbAzNpH', '2018-01-14 14:06:18', '2018-01-14 14:21:00'),
(23, 'Anant Gupta', 'Sr PHP Developer', 'anant5@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$veW65/SREqXqX9ULL0DLouoX3Iqwq6FsZkMmkEhykR/T/DQS8F54S', 'Inactive', 'User', NULL, NULL, '2018-01-15 17:15:31', '2018-01-15 17:15:31'),
(24, 'Anant Gupta', 'test', 'anil_seller@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$3lJe6lorubgaVfUZM9AMC.Dr2yvLIsH1PMSKR9riuEoRRjTOL78AS', 'Inactive', 'Seller', NULL, NULL, '2018-01-16 17:24:46', '2018-01-16 17:24:46'),
(25, 'Matty ker', 'feature/ABEP-1918', 'johnh@gmail.com', NULL, NULL, NULL, NULL, NULL, '$2y$10$rEqsftivBYvlRngNLyp5XufIklW3uQqL/c4ddkivCsiZ645zovxUa', 'Inactive', 'Seller', NULL, NULL, '2018-01-18 09:44:44', '2018-01-18 09:44:44'),
(27, 'Anil', 'DON', 'anil123@gmail.com', NULL, NULL, '8985748578', 'www.website.com', 'Bhopal', '$2y$10$Ia0FpGusy92PSjXQWeP6gu8hKRrMmttYslSOrCLsWMPLEotyqAVbu', 'Inactive', 'Seller', 'profile/2ce4667914ddaf8a2c45cc3b17eee1de.jpg', 'ksEJjpzgAF8XI7ggPpXjGXu8CAw3ln4UOqKaeCZ7x8XP1bPmFvvYsnaE9eSW', '2018-01-18 15:22:37', '2018-01-18 15:27:18'),
(28, 'Anil', NULL, 'TEST@GMAIL.COM', NULL, NULL, '9300358503', NULL, NULL, '$2y$10$1quuIckN18h1KLn7KsdlM.wcoKFUCB/Ba5tdaHhwGdjueRgq.gG0.', 'Inactive', 'User', NULL, 'gBIzeJbgbYs0Ik0OLiq2eFiriDIkHQRzwxEPB3aTSmVUGTMYnwb0w2KL28ZS', '2018-01-18 15:29:40', '2018-01-18 15:29:40'),
(29, 'Yesy', NULL, 'test7@gmail.com', NULL, NULL, '9897979779', NULL, NULL, '$2y$10$z6r9MUysjyb8xxxhI6uTr./8L.58HK8aIpg6puvzzrkWtxiWe1ORm', 'Inactive', 'User', NULL, 'jDK9FRlrBrErsc20OovoVWzkvmeXDNCfz2y9i9zhAetzKOOkfI4VkxuqM6g4', '2018-01-18 15:54:34', '2018-01-18 15:54:34'),
(30, 'test', NULL, 'test8@gmail.com', NULL, NULL, '9323434232', NULL, NULL, '$2y$10$/8SK1nM/b1/7FrDHRBdXkeYeYRIvxfkdpRKqqP4UgE/40e5QVRdnu', 'Inactive', 'Seller', NULL, NULL, '2018-01-18 15:55:43', '2018-01-18 15:55:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applied_candidate`
--
ALTER TABLE `applied_candidate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_attachment`
--
ALTER TABLE `email_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_user_id_foreign` (`user_id`),
  ADD KEY `reviews_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `seller_address`
--
ALTER TABLE `seller_address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `seller_details`
--
ALTER TABLE `seller_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_offer`
--
ALTER TABLE `seller_offer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `seos`
--
ALTER TABLE `seos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siteconfiguration`
--
ALTER TABLE `siteconfiguration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applied_candidate`
--
ALTER TABLE `applied_candidate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `email_attachment`
--
ALTER TABLE `email_attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seller_address`
--
ALTER TABLE `seller_address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `seller_details`
--
ALTER TABLE `seller_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `seller_offer`
--
ALTER TABLE `seller_offer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `seos`
--
ALTER TABLE `seos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `siteconfiguration`
--
ALTER TABLE `siteconfiguration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `applied_candidate` (`id`),
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `applied_candidate` (`id`),
  ADD CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
