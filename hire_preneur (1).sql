-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2017 at 12:54 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hire_preneur`
--

-- --------------------------------------------------------

--
-- Table structure for table `applied_candidate`
--

CREATE TABLE `applied_candidate` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coverletter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipdata` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `comments` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, 'How to register?', '<p>Register directlysss...</p>', '1', '2017-09-06 20:18:20', '2017-09-06 14:48:20');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_08_01_130814_applied_candidate', 1),
(2, '2017_08_01_151844_users', 1),
(3, '2017_08_01_151917_password_resets', 1),
(4, '2017_08_02_144847_comments', 1),
(5, '2017_08_02_192246_ratings', 1),
(6, '2017_08_03_161729_add_ip_applied_candidate', 1),
(7, '2017_08_03_171045_reviews', 1),
(8, '2017_09_05_182542_posts', 1),
(9, '2017_09_05_182614_teams', 1),
(10, '2017_09_05_182633_testimonial', 1),
(11, '2017_09_06_194240_faqs', 1),
(12, '2017_09_08_173112_seos', 1),
(13, '2017_09_11_174954_post_comments', 1),
(14, '2017_09_11_215703_galleries', 1),
(15, '2017_09_12_200434_siteconfiguration', 1),
(16, '2017_09_12_213943_sociallinks', 1),
(17, '2017_09_13_174128_sliders', 1),
(18, '2017_09_14_141317_product_categories', 1),
(19, '2017_09_14_141326_products', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('shuklayogesh065@gmail.com', '$2y$10$HYwpLTSUkC7vAAcJ0TNVnueyYGldlGQd3AnuTxsALDoRHtu6P8102', '2017-12-30 14:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `author`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'aaaa', '1', '<p>aaaaa</p>', '1504647300.png', 'Active', '2017-09-05 16:05:00', '2017-09-05 16:34:13'),
(3, 'aaasasas', '1', 'aasasas', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `cat_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Television', '1', '2017-09-15 17:31:59', '2017-09-15 12:01:59'),
(2, 'Mobile', '0', '2017-09-15 17:32:23', '2017-09-15 12:02:23'),
(4, 'Refrigirator', '1', '2017-09-15 12:03:49', '2017-09-15 12:03:49');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `candidate_id` int(10) UNSIGNED NOT NULL,
  `ratings` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `candidate_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seller_details`
--

CREATE TABLE `seller_details` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `about_me` text,
  `experience` text,
  `skills` text,
  `industries` text,
  `degrees` varchar(255) NOT NULL,
  `award_certification` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_details`
--

INSERT INTO `seller_details` (`id`, `user_id`, `about_me`, `experience`, `skills`, `industries`, `degrees`, `award_certification`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, NULL, NULL, NULL, NULL, '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `seos`
--

CREATE TABLE `seos` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seos`
--

INSERT INTO `seos` (`id`, `page_name`, `title`, `keywords`, `description`, `created_at`, `updated_at`) VALUES
(1, 'About', 'About Us', 'About Us', '<p>About Us</p>', '2017-09-11 13:31:55', '2017-09-11 08:01:55'),
(2, 'Term And Condition', 'Term & Condition', 'Term & Condition', '<p>Term &amp; Condition</p>', '2017-09-11 13:31:31', '2017-09-11 08:01:31'),
(3, 'Privacy policy', 'Privacy policy', 'Privacy policy', '<p>Privacy policy</p>', '2017-09-08 22:10:28', '2017-09-08 16:40:28'),
(4, 'Contact', 'contact us', 'contact us', '<p>contact us</p>', '2017-09-08 22:09:36', '2017-09-08 16:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `siteconfiguration`
--

CREATE TABLE `siteconfiguration` (
  `id` int(10) UNSIGNED NOT NULL,
  `website_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siteconfiguration`
--

INSERT INTO `siteconfiguration` (`id`, `website_name`, `website_title`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Laravel', 'A laravel site', 'logo.png', NULL, '2017-09-12 21:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'my first slider', 'my first slider my first slider my first slider my first slider my first slider my first slider', '1505337953.png', '1', '2017-09-13 21:25:53', '2017-09-13 21:35:18'),
(2, 'my second blog', 'my second blog my second blog my second blog my second blog my second blog my second blog my second blog', '1505338578.png', '1', '2017-09-13 21:36:18', '2017-09-13 21:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `sociallinks`
--

CREATE TABLE `sociallinks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sociallinks`
--

INSERT INTO `sociallinks` (`id`, `name`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Facebook', 'http://www.facebook.com', 1, '2017-09-13 04:12:12', '2017-09-13 13:08:32'),
(2, 'Twitter', 'http://www.twitter.com', 1, '2017-09-13 13:35:24', '2017-09-13 16:27:04'),
(3, 'Pintrest', 'http://www.pintrest.com', 1, '2017-09-13 15:52:00', '2017-09-13 16:26:57'),
(4, 'Instagram', 'http://www.instagram.com', 1, '2017-09-13 15:55:15', '2017-09-13 15:55:15');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `designation`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'team 1', 'team 1', '<p>team 1</p>', '1504722796.JPG', 'Inactive', '2017-09-06 13:03:16', '2017-09-06 13:17:49'),
(2, 'aa', 'aa', '<p>aaa</p>', '1504723571.JPG', 'Active', '2017-09-06 13:16:11', '2017-09-06 13:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Testimonial 11', '<p>Testimonialm11</p>', '1504720631.jpg', 'Inactive', '2017-09-06 12:27:11', '2017-09-06 12:33:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `title`, `email`, `mobile`, `website`, `address`, `password`, `status`, `role`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Yogesh Shukla', NULL, 'shuklayogesh065@gmail.com', '', '', '', '$2y$10$f/Wk8bM2nmq7TxcHGUc8M.qVgVE/bwaaGD2mEf4EbDQgTMTyuqkBe', 'Active', 'Admin', '1505319681.JPG', 's3BJMk4fuUrAJ3fVLiY8Vex6xSjnRrDFfhO3ZUQjRpvgiSZXapmSeFvwQkwh', '2017-09-05 18:30:00', '2017-09-15 13:39:01'),
(2, 'Astha Sharma', NULL, 'asthasharma1609@gmail.com', '', '', '', '$2y$10$byIwHawQHgsEGR7wFzVsKOQVXJgLgU/waQT2kafSeUUNqsBtytKIa', 'Active', 'User', '', 'l8AUHBEKxkbL3qyVhsf7cawNmYq63x6Dqf4UNTDW', '2017-09-11 10:56:36', '2017-09-11 10:57:05'),
(3, 'Ekart Logistic', NULL, 'ekart@gmail.com', '', '', '', '$2y$10$tYDuGpyKSOphv/ifmtcsb.wS.LaRluYSVXPUERZESaQtpriGqa4sm', 'Active', 'Seller', '', 'R6JphnOf3iHSx789ExXQkwyd5JIyEi2zx3HE8Lz4', '2017-09-15 13:42:56', '2017-09-15 13:43:22'),
(5, 'yogesh shukla', NULL, 'yogeshshukla@gmail.com', '', '', '', '$2y$10$XYIHkOwxuRMv0UCB4i5Vx.N1ldKQskHkNnYeXAxyc4bzslcUsJIo6', NULL, NULL, NULL, NULL, '2017-12-30 09:37:53', '2017-12-30 09:37:53'),
(6, 'yogesh shukla', 'pandit', 'shuklayogesh065@gmail.com', '', '', '', '123456', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:39:01', '2017-12-30 14:39:01'),
(7, 'yogesh shukla', 'pandit', 'shuklayogesh065@gmail.com', '', '', '', '$2y$10$SNlQ7iZzjHA4yF7nqMTXq.yCuazgbw77kouqstUGDl5xhvM1N4z8S', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:41:18', '2017-12-30 14:41:18'),
(8, 'yogesh shukla', 'pandit', 'yogeshshukl4a@gmail.com', '', '', '', '$2y$10$0TQ83rn4DTmD0eq..nF0ae3rnsBqCEzVLV/2abp.z41yzauunliLK', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:45:08', '2017-12-30 14:45:08'),
(9, 'yogesh shukla', 'pandit', 'shuklayogesh0645@gmail.com', '', '', '', '$2y$10$REVDzyG9Q6WPCxZJlZ0NKeD17O/ttjNlDDxHCV7IMDnIhAy0Nmhmu', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:45:54', '2017-12-30 14:45:54'),
(10, 'yogesh shukla', 'pandit', 'shuklayogesh078645@gmail.com', '', '', '', '$2y$10$Lh7.PXcbqtK29ERKILsGoeBloFUZtrbwJ9QIa9/RwOCgu5JOYVRhG', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:48:36', '2017-12-30 14:48:36'),
(11, 'yogesh shukla', 'pandit', 'shuklayogesh067895@gmail.com', '', '', '', '$2y$10$VTqFYvCoECvvGffYZ8C6P.xDUGy/trfIuJts97VrxoC.ebAc6Azci', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:52:10', '2017-12-30 14:52:10'),
(12, 'yogesh shukla', 'pandit', 'yogeshshhhhukla@gmail.com', '', '', '', '$2y$10$8y2rtYbqp59HX11hiKVI/e0QTDT8vtrbs6XRoNTNWYChvmYWaLBPK', 'Inactive', 'Seller', NULL, NULL, '2017-12-30 14:53:19', '2017-12-30 14:53:19'),
(13, 'yogesh shukla', 'pandit', 'shuklayogesh0656@gmail.com', '', '', '', '$2y$10$WLlJF4D4BKGJXA3TZ8n4WeE4f0.u8K8ByWqnVxNODaCDfLIXx2/.m', 'Inactive', 'Seller', NULL, 'rBqNPKre7Lv0ICFBCUZ2hXlePkQQFBAyUxKUjsg7xgluy1DoeYhM8Kl0jrTE', '2017-12-30 14:56:47', '2017-12-30 14:56:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applied_candidate`
--
ALTER TABLE `applied_candidate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_user_id_foreign` (`user_id`),
  ADD KEY `reviews_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `seller_details`
--
ALTER TABLE `seller_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seos`
--
ALTER TABLE `seos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siteconfiguration`
--
ALTER TABLE `siteconfiguration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applied_candidate`
--
ALTER TABLE `applied_candidate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seller_details`
--
ALTER TABLE `seller_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `seos`
--
ALTER TABLE `seos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `siteconfiguration`
--
ALTER TABLE `siteconfiguration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `applied_candidate` (`id`),
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `applied_candidate` (`id`),
  ADD CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
