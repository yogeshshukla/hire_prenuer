@include('layouts.header')

<section class="form-div">
  <div class="container">
    <div class="row">
    <h3><center> User Login</center></h3>
    </div>
    <div class="offer-form">
                  <div id="legend">
                       <!--  <a href="{{ url('/auth/facebook') }}">  <button class="button-1"><i class="fa fa-facebook-square"></i> Continue with Facebook</button></a>
                         <a href="{{ url('/auth/twitter') }}">  <button class="button-2"><i class="fa fa-twitter-square"></i> Continue with Twitter</button></a>--> 
              <a href="{{ url('/auth/linkedin') }}">
  						 <button class="socialBtn"><i class="fa fa-linkedin"></i></button>						
              </a>
						</div> 
                      <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                         {{ csrf_field() }}
                        <fieldset>
                          
                          <div class="link-div">
                          <span>OR</span>
                          </div>

                          @if (session('account_created'))
                            <span class="help-block">
                                <strong>{{ session('account_created') }}</strong>
                            </span>
                          @endif
                          @if ($errors->any())
                              <span class="help-block">
                                <strong>{{ $errors->first() }}</strong>
                              </span>
                          @endif

                          <div class="form-w">
              <input placeholder="Email" type="email" name="email" class="text1"  value="{{ old('email') }}" required autofocus>
               @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
              <input placeholder="Password" type="password" name="password" class="text1" required="" style="margin-bottom:0px;">
                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              <div class="checkbox reamber">
              <label>
                <input type="checkbox"> Stay Signed in
              </label>
              <label class="float-right">
                <a href="#" class="forgot">Forgot Password ?</a>
              </label>
              </div>
                          <div class="control-group">
                            <div class="text-center">
                              <center><button class="searchBtn width130 mtop50"> Login</button></center>
                            </div>
                          </div>
                          
                          </div>
                          
                        </fieldset>
                      </form>                
                    </div>
                    
                </div>
     
</section>


@include('layouts.footer')