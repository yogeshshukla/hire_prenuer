@include('layouts.header')
<section class="form-div">
<div class="container">
<div class="row">
<h2>Register and create your account</h2>
</div>
		  <div class="offer-form">

@if ($errors->any())
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
						@endif
<form class="form-horizontal" method="POST" action="{{ url('/auth/register') }}">
                        {{ csrf_field() }}
  
     	     <ul class="contact-list">
		    <li> 
			  <label>Full Name</label>
    <input id="name" type="text" class="text1" name="name" value="{{ old('name') }}" required autofocus>

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    				</li>
			<li> 
			  <label>Title</label>

		    <input id="title" type="text" class="text1" name="title" class="text1" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
  			</li>
			<li>
			   <label>Email</label>

    	<input id="email" type="email" class="text1" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
			</li>
			<li>
			   <label>Password</label>


	    <input id="password" type="password" class="text1" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
			</li>
			<li>
			   <label>Confirm Password</label>

    <input id="password-confirm" type="password" class="text1" name="password_confirmation" required>

  <input type="hidden" name="seller" value="{{$seller}}">
    			</li>
		 <ul>				

 <center>     <button type="submit" class="searchBtn width130 mtop50">SAVE</button></center>

</form>

</div>
</div>
</section>
@include('layouts.footer')