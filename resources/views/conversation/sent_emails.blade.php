@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Mail</div>
                 <div class="panel-body">    
					<table border="1"> 
						<tr>
						<td>Receiver</td>
						<td>Subject</td>
						<td>Message</td>
						<td>Attachment</td>
						</tr>						
						@foreach($data as  $indexKey => $val)
						<tr>
							<td>{{$val->receiver_email}}</td>
							<td>{{$val->subject}}</td>
							<td>{{$val->body}}</td>
							<td>image</td>
						</tr>
						@endforeach
					</table>					

                        </div>
        </div>
    </div>
</div>
</div>

@endsection
