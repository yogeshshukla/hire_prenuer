@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Receued Mail</div>
                 <div class="panel-body">    
					<table border="1"> 
						<tr>
							<td>-</td>
						<td>Sender</td>
						<td>Subject</td>
						<td>Message</td>
						<td>Attachment</td>
						</tr>						
						@foreach($data as  $indexKey => $val)
						<tr>
							<td><input type="checkbox" name="delete[]" class="delete" id="{{$val->id}}"></td>
							<td>{{$val->sender_email}}</td>
							<td>{{$val->subject}}</td>
							<td>{{$val->body}}</td>
							<td>image</td>
						</tr>
						@endforeach
					</table>					

                        </div>
        </div>
    </div>
</div>
</div>

@endsection
