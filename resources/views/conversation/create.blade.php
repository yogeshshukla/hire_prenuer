@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Mail</div>

                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ route('post_mail') }}">
                        {{ csrf_field() }}

<!--                         <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('receiver') }}" required autofocus>

                                @if ($errors->has('receiver'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('receiver') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

 -->                        <div class="form-group{{ $errors->has('receiver') ? ' has-error' : '' }}">
                            <label for="receiver" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="receiver" value="{{ old('receiver') }}" required>

                                @if ($errors->has('receiver'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('receiver') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject" class="col-md-4 control-label">Subject</label>

                            <div class="col-md-6">
                                <input id="subject" type="text" class="form-control" name="subject" required>

                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="message" class="col-md-4 control-label">Message</label>

                            <div class="col-md-6">
                                <input id="message" type="text" class="form-control" name="message" required>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('attachment') ? ' has-error' : '' }}">
                            <label for="subject" class="col-md-4 control-label">Attachment</label>

                            <div class="col-md-6">
                                <input id="attachment" type="file" class="form-control" name="attachment" >

                                @if ($errors->has('attachment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('attachment') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
