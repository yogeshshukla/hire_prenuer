@include('layouts.header')

<section class="form-div">
    <div class="container">
        <div class="row">
        <h3><center>Make Your Offer</center> </h3>
        </div>
  <div class="offer-form">
			@if ($errors->any())
				<span class="help-block">
					<strong>{{ $errors->first() }}</strong>
				</span>
			@endif    
			<form  method="POST" action="{{ url('/auth/seller_offer') }}">
             {{ csrf_field() }}
                     <ul class="contact-list">
            <li> 
              <label>Project Name</label>
            <input type="hidden" name="user_id" value="{{$user_id}}">
		  <input  type="text" class="text1" name="project_name" required="">
		  @if ($errors->has('project_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('project_name') }}</strong>
                </span>
            @endif
                        </li>
            <li> 
              <label>Project Description</label>
		  <input  type="text" class="text1" name="project_description" required="">
		  @if ($errors->has('project_description'))
                <span class="help-block">
                    <strong>{{ $errors->first('project_description') }}</strong>
                </span>
            @endif
                        </li>
            <li>
               <label>List what it includes (Keep within 2-3 lines)</label>
		  <textarea rows="2" cols="50" class="text3" name="includes" required=""> </textarea>
		  @if ($errors->has('includes'))
                <span class="help-block">
                    <strong>{{ $errors->first('includes') }}</strong>
                </span>
            @endif
		  </li>
            <li>
               <label>List what does NOT include ( Keep within 2-3 line )</label>
               <textarea rows="2" cols="50" class="text3" name="excludes" required=""> </textarea>
		  @if ($errors->has('excludes'))
                <span class="help-block">
                    <strong>{{ $errors->first('excludes') }}</strong>
                </span>
            @endif
            </li>
            <li>
               <label>Notes</label>
               <textarea rows="2" cols="50" class="text3" name="notes" required=""> </textarea>
          @if ($errors->has('notes'))
                <span class="help-block">
                    <strong>{{ $errors->first('notes') }}</strong>
                </span>
            @endif
            </li>
            <li> 
              <label>Price</label>
            
            <input  type="text" class="text1" name="price" required="">
		  @if ($errors->has('price'))
                <span class="help-block">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
             @endif
		  </li>
            <li>
               <label>Discount</label>
            
          <input  type="text" class="text1" name="discount_price" required="">
          @if ($errors->has('discount_price'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount_price') }}</strong>
                </span>
            @endif
            </li>
            </ul>
          <center>
		  <button type="submit" class="searchBtn width130 mtop50">Save</button> </a> </center>
</form>
 		</div>
	</div>
</div>
@include('layouts.footer')