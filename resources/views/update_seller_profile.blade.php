@include('layouts.header')    
	<section class="form-div">
<div class="container">
<div class="row">
<h3 style="padding-bottom:0px;"><center>Complete Your professional profile </center></h3>
<h5>This is what visitors will learn about you.</h5>
</div>
<div class="row">
<div class="col-sm-3">

<div class="img-upload-div">
<div class="upload-img">
@if($data->image != "")
<img src="{{url('storage/app/'.$data->image)}}" />
@else
<img src="{{url('public/assets/img/users/default.jpg')}}" />
@endif
</div>
</div>
<div class="con">
<form class="form-horizontal" method="POST" action="{{ url('update-profile') }}" enctype="multipart/form-data">
<p>Upload a 200 x 400 image to use for your profile.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
<!--<button type="submit" class="btn btn-default-button">UPLOAD</button> -->
<input type="file" name="profile_pic" class="form-control" >
                                
                                @if ($errors->has('profile_pic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile_pic') }}</strong>
                                    </span>
                                @endif
</div>
</div>
<div class="col-sm-9">

    {{ csrf_field() }}
<div class="ragister-3">
   <!--  @if(Auth::check())
  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" >Name</label>

                               <input id="name" type="text" class="form-control" name="name" value="{{ !empty(Auth::user()->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
    </div>
	<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" >Title</label>

                                <input id="title" type="text" class="form-control" name="title" value="{{ !empty(Auth::user()->title) }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            
    </div>
	                       
	<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="Address" >Address</label>

                           <textarea name="address" class="form-control">{{ !empty(Auth::user()->address) }}</textarea>
                                
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                           
    </div>
  @endif  --> 
  @if($seller)	
  <div class="form-group ">
    <label>MARKETING SUMMARY <span>(Limit to 500 Characters).</span></label>
        <textarea name="marketing_summary" class="form-control height-auto" rows="4">{{ !empty($data->about_me)?$data->about_me:""  }}</textarea>
                                
                                @if ($errors->has('marketing_summary'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('marketing_summary') }}</strong>
                                    </span>
                                @endif
  </div>
  <div class="form-group">
    <label>SKILLS <span>(Limit to 150 Characters).</span></label>
       <textarea name="skills" class="form-control height-auto" rows="3">{{ !empty($data->skills)?$data->skills:"" }}</textarea>
                                
                                @if ($errors->has('skills'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('skills') }}</strong>
                                    </span>
                                @endif
  </div>
    <div class="form-group">
    <label>INDUSTRIES <span>(Limit to 100 Characters).</span></label>
        <textarea name="industry" class="form-control height-auto" rows="3">{{ !empty($data->industries)?$data->industries:"" }}</textarea>
                                
                                @if ($errors->has('industry'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('industry') }}</strong>
                                    </span>
                                @endif
  </div>
  <div class="form-group">
    <label>EXPERIENCE <span>(List your top 5 companies you have worked for/with. Limit to 350 Characters).</span></label>
       <textarea name="experience" class="form-control height-auto" rows="4">{{!empty($data->experience)?$data->experience:"" }}</textarea>
                                
                                @if ($errors->has('experience'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                                @endif
  </div>
    <div class="form-group">
    <label>CAREER DEGREE <span>(Limit to 100 Characters).</span></label>
       <textarea name="career_degree" class="form-control height-auto" rows="2">{{ !empty($data->degrees)?$data->degrees:""  }}</textarea>
                                
                                @if ($errors->has('career_degree'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('career_degree') }}</strong>
                                    </span>
                                @endif
  </div>
    <div class="form-group">
    <label>AWARDS AND RECOGNITIONS <span>(Limit to 500 Characters).</span></label>
       <textarea name="awards" class="form-control height-auto" rows="3">{{ !empty($data->award_certification)?$data->award_certification:""}}</textarea>
                                
                                @if ($errors->has('awards'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awards') }}</strong>
                                    </span>
                                @endif
  </div>
  <input type="hidden" name="seller" value="Seller">
  <input type="hidden" name="user_id" value="{{$user_id}}">
                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                    <label for="mobile" >Mobile</label>

                        <input id="mobile" type="text" class="form-control" name="mobile" value="{{ !empty($data->mobile)?$data->mobile:''}}" maxlength="20" required>

                        @if ($errors->has('mobile'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                        @endif
                   
            </div>
        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                <label for="Website" >Website</label>

                    <input id="website" type="text" class="form-control" name="website" value="{{ !empty($data->website)?$data->website:''}}" >

                    @if ($errors->has('website'))
                        <span class="help-block">
                            <strong>{{ $errors->first('website') }}</strong>
                        </span>
                    @endif
     </div>

  @endif
    <button type="submit" class="searchBtn float-right">SAVE</button>
</form>
</div>
</div>
</div>
</div>
<br>
</section>

@include('layouts.footer')