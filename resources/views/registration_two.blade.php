@include('layouts.header')
<section class="form-div">
	<div class="container">
		<div class="row">
		<h3><center> STEP 2 </center></h3>
		</div>
		<div class="offer-form">
			@if ($errors->any())
								<span class="help-block">
									<strong>{{ $errors->first() }}</strong>
								</span>
						@endif
			<form  method="POST" action="{{ url('/auth/seller_adddress') }}">
             {{ csrf_field() }}
        <ul class="contact-list">
		    <li> 
			  <label>Address 1</label>
		     <input type="hidden" value="{{$user_id}}" name="user_id">
			<input  type="text" name="house_no" class="text1" required="">
            @if ($errors->has('house_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('house_no') }}</strong>
                </span>
            @endif
			</li>
			<li> 
			  <label>Address 2</label>
			<input  name="locality" type="text" class="text1" required="">
			 @if ($errors->has('locality'))
                <span class="help-block">
                    <strong>{{ $errors->first('locality') }}</strong>
                </span>
            @endif
			</li>
			<li>
			   <label>City</label>
			<input  type="text" name="city" class="text1" required="">			
			 @if ($errors->has('city'))
                <span class="help-block">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
            @endif
			</li>
			<li>
			   <label>Zip Code</label>
			<input  type="text" name="zip" class="text1" required="">
			 @if ($errors->has('zip'))
                <span class="help-block">
                    <strong>{{ $errors->first('zip') }}</strong>
                </span>
            @endif
			</li>
			<li> 
			  <label>State/Regions</label>
			<input  type="text" name="state" class="text1" required="">
			 @if ($errors->has('state'))
                <span class="help-block">
                    <strong>{{ $errors->first('state') }}</strong>
                </span>
            @endif
		
			<input  type="hidden" name="country" class="text1" value="US" required="">
			 @if ($errors->has('country'))
                <span class="help-block">
                    <strong>{{ $errors->first('country') }}</strong>
                </span>
            @endif
			</li>
		 <ul>				
		 
			<center>
		     <button type="submit" class="searchBtn width130 mtop50">SAVE</button> 
		   </center>
		</form>
		</div>
	</div>
</section>


@include('layouts.footer')
