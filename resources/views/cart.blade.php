@include('layouts.header')
<section class="form-div">
<div class="container">

<div class="row">
<h3 style="padding-bottom:0px;"><center>Check Your Cart</center></h3>

</div>
<br/>
	<table id="cart" class="table table-hover table-condensed">
		<thead>
			<tr>
				<th style="width:50%">Product</th>
				<th style="width:10%">Price</th>
				<th style="width:8%">Quantity</th>
				<th style="width:22%" class="text-center">Subtotal</th>
				<th style="width:10%"></th>
			</tr>
		</thead>
		<tbody>
			@if(!$data->isEmpty())
			@foreach($data as $val)
		
			<tr>
				<td data-th="Product">
					<div class="row">
						<div class="col-sm-2 hidden-xs pleft0"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
						<div class="col-sm-10">
							<h4 class="nomargin">{{$val->name}}</h4>
							<p>{{$val->attributes['project_description']}}</p>
						</div>
					</div>
				</td>
				<td data-th="Price">${{$val->price}}</td>
				<td data-th="Quantity">
					<input type="number" readonly="readonly" class="form-control text-center" value="{{$val->quantity}}">
				</td>
				<td data-th="Subtotal" class="text-center">${{$total}}</td>
				<td class="actions text-right" data-th="">
					<button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
					<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>								
				</td>
			</tr>
			@endforeach
			@else
			<tr>
				<td colspan="5">
				<center><h5> No services added. </h5></center>
				</td>
			</tr>
			@endif
		</tbody>
		<tfoot>
			<tr class="visible-xs">
				<td class="text-center"><strong>Total </strong></td>
			</tr>
			<tr>
				<td><a href="{{url('/profile')}}" class="btn searchBtn"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
				<td colspan="2" class="hidden-xs"></td>
				<td class="hidden-xs text-center"><strong>Total ${{$stotal}}</strong></td>
				<td><a href="#" class="btn searchBtn">Checkout <i class="fa fa-angle-right"></i></a></td>
			</tr>
		</tfoot>
	</table>

</div>
</section>
@include('layouts.footer')


