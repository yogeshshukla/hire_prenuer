<?php //print_r($data->user_id); exit;?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Profile</title>
<link href="public/front/font/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="public/front/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="public/front/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="page-content" class="candidate-profile
<nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
								<li><a href="{{ url('seller-profile') }}">Profile</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
          <div class="page-content mt30 mb30">
            <div class="">


              <div class="tab-pane active" id="candidate-profile">
                <div class="row">
                  <div class="col-md-4">
                    <div class="motijob-sidebar">
                      <div class="candidate-profile-picture">
                        <img src="{!! Auth::user()->profilePhoto() !!}" alt="">
					  </div> <!-- end .agent-profile-picture -->

                      <!--
                        <div class="title">
                          <h6>General Information</h6>
                        </div> 

                        <ul class="list-unstyled">
                          <li><strong>Birthday:</strong>June 17, 1986</li>
                          <li><strong>Address:</strong>New York City, United States</li>
                          <li><strong>Phone:</strong>+1 123-456-7890</li>
                          <li><strong>Email:</strong>email@example.com</li>
                          <li><strong>Website:</strong>www.example.com</li>
                          <li class="mt20"><strong>Experience:</strong>5 Years</li>
                          <li><strong>Degree:</strong>MBA</li>
                          <li><strong>Residence:</strong>USA</li>
                        </ul>
-->
                        <!-- social link -->

                        <div class="title mt20">
                          <h6>Socialize</h6>
                        </div> <!-- end .title -->

                        <ul class="list-inline social-link mt10">
                          <li class="envelop-color"><a href="#"><i class="fa fa-envelope"></i></a></li>
                          <li class="facebook-color"><a href="#"><i class="fa fa-facebook"></i></a></li>
                          <li class="google-color"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                          <li class="twitt-color"><a href="#"><i class="fa fa-twitter"></i></a></li>
                          <li class="linked-color"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                          <li class="pinterest-color"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>


                      </div> <!-- end .candidate-general-info -->
                    </div>
					<div class="col-md-8">
                    

                      <form class="form-horizontal" method="POST" action="{{ url('update-seller-profile') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ Auth::user()->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						-->
						<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ Auth::user()->mobile }}" required>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="Website" class="col-md-4 control-label">Website</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" value="{{ Auth::user()->website }}" >

                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="Address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
							<textarea name="address" class="form-control">{{ Auth::user()->address }}</textarea>
                                
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('marketing_summary') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Marketing Summary</label>

                            <div class="col-md-6">
							<textarea name="marketing_summary" class="form-control">{{ $data->about_me }}</textarea>
                                
                                @if ($errors->has('marketing_summary'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('marketing_summary') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Experience</label>

                            <div class="col-md-6">
							<textarea name="experience" class="form-control">{{ $data->experience }}</textarea>
                                
                                @if ($errors->has('experience'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('skills') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Skills</label>

                            <div class="col-md-6">
							<textarea name="skills" class="form-control">{{  $data->skills  }}</textarea>
                                
                                @if ($errors->has('skills'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('skills') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('industry') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Industries</label>

                            <div class="col-md-6">
							<textarea name="industry" class="form-control">{{ $data->industries }}</textarea>
                                
                                @if ($errors->has('industry'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('industry') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('career_degree') ? ' has-error' : '' }}">
                            <label for="Career Degree" class="col-md-4 control-label">Career Degree</label>

                            <div class="col-md-6">
							<textarea name="career_degree" class="form-control">{{ $data->degrees }}</textarea>
                                
                                @if ($errors->has('career_degree'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('career_degree') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('awards') ? ' has-error' : '' }}">
                            <label for="Awards" class="col-md-4 control-label">Awards and Recognitions</label>

                            <div class="col-md-6">
							<textarea name="awards" class="form-control">{{ $data->award_certification  }}</textarea>
                                
                                @if ($errors->has('awards'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awards') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('profile_pic') ? ' has-error' : '' }}">
                            <label for="Profile Picture" class="col-md-4 control-label">Profile Picture</label>

                            <div class="col-md-6">
							<input type="file" name="profile_pic" class="form-control" >
                                
                                @if ($errors->has('profile_pic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profile_pic') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Profile
                                </button>
                            </div>
                        </div>
                    </form>

					  
					  

                    </div> <!-- end .candidate-description -->
                  </div> <!-- end .3col grid layout -->

                  
                  </div> <!-- end .9col grid layout -->

                </div> <!-- end .row -->
              </div> <!-- end .tabe pane -->



            </div> <!-- end .responsive-tabs.dashboard-tabs -->

          </div> <!-- end .page-content -->
        </div> <!-- end .container -->
      </div>
<script src="public/front/js/jquery-1.12.4.min.js" type="text/javascript"></script> 
<script src="public/front/js/bootstrap.js" type="text/javascript"></script> 
<script>
$(document).ready(function() {
    $('#Carousel').carousel({
        interval: 5000
    })
});
</script>
</body>
</html>
