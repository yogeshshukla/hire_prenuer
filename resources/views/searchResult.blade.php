@include('layouts.header')
<section class="color-div">
<div class="container">
<div class="row">
<h3>
Hire professional independent <br/>
consultants from various industries.
</h3>
	<center>
		@include('includes.search')
	</center>
</div>
</div>
</section>


<section class="HomeMainBox">
  <div class="container">

    <!---box--->
	<div class="col-md-12 p0">
	   <h2 class="headding1" style="border-top:none"></h2>
	 @if(!$data->isEmpty())
	  @foreach($data as $val)
	<div class="col-md-3 pleft0">
		  <div class="card box2">
			<a href="{{url('/profile/'.$val->user_id)}}">
			@if($val->image!= "" and file_exists('storage/app/'.$val->image)) 
			<img class="card-img-top user-profile" src="{{url('storage/app/'.$val->image)}}" alt="" > 
			@else
			<img class="card-img-top user-profile" src="public/assets/img/users/default.jpg" alt="" >
			@endif
			</a>
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i>{{$val->price}}</p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>24</strong>
				</div>
			</div>
		  </div>
		</div>
	@endforeach
	@else
		<center><h3> No data found. </h3></center>
	@endif
  </div>
</section>

@include('layouts.footer')