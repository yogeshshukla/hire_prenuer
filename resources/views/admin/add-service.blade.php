@extends('admin/includes.layout')
@section('content')


                 <!-- Page content -->
                     <div id="page-content">
                        
                        <!-- eCommerce Dashboard Header -->
                            @include('admin/product-header')
                        <!-- END eCommerce Dashboard Header -->

                        <!-- Quick Stats 
                           -->
                        <!--END Quick Stats -->

                        <!-- All Products Block -->
                        <div class="block full">
                            <!-- All Products Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                    <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                </div>
                                <h2><strong>Add</strong> Service</h2>
                            </div>
                            <!-- END All Products Title -->

                            <!-- General Data Content -->
                                    <!--  -->
                                     <div class="form-horizontal form-bordered">

                                        @if (session('confirm'))
                                             <div class="alert alert-success text-center">{{ session('confirm') }}</div>
                                        @endif
                                        @if (session('message'))
                                                <h4 class="alert alert-danger text-left"> {{ session('message') }} </h4>
                                        @endif

                                        {{ Form::open(array('url' => '/admin/add-service', 'method' => 'post','files' => true)) }}

                                         <div class="form-group">
                                            <label class="col-md-3 control-label" for="product-category">Category <span style="color: red">*</span></label>
                                            <div class="col-md-8">
                                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                                <select id="product-category" name="cat_id" class="select-chosen" data-placeholder="Choose Category.." style="width: 250px;">
                                                     <option></option>
                                                   @foreach ($data as $category)
                                                    <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="category-name">Name</label>
                                            <div class="col-md-9">
                                             {{ Form::text('service', '',$attributes = array('placeholder'=>'Enter Service name..','class'=>"form-control",'id'=>"service-name") ) }} 

                                             @foreach($errors->get('service')  as $message)
                                                <p class="alerterror"> {{ $message }} </p>
                                            @endforeach

                                            </div>
                                            
                                        </div>
                                       

                                         <div class="form-group">
                                            <label class="col-md-3 control-label" for="category-name">Description</label>
                                            <div class="col-md-9">
                                             {{ Form::text('description', '',$attributes = array('placeholder'=>'Enter Service info','class'=>"form-control",'id'=>"description") ) }} 

                                             @foreach($errors->get('service')  as $message)
                                                <p class="alerterror"> {{ $message }} </p>
                                            @endforeach

                                            </div>
                                            
                                        </div>

                                         <div class="form-group">
                                            <label class="col-md-3 control-label" for="category-name">Image</label>
                                            <div class="col-md-9">
                                             <input type="file" name="image" id="image">
                                             @foreach($errors->get('image')  as $message)
                                                <p class="alerterror"> {{ $message }} </p>
                                            @endforeach

                                            </div>
                                            
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Published?</label>
                                            <div class="col-md-9">
                                                <label class="switch switch-primary">
                                                     {{ Form::checkbox('status', '1',$attributes = array('class'=>"form-control",'id'=>"service-status",'checked'=>"checked") ) }}
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="col-md-3 control-label"></label>
                                            <div class="col-md-9">
                                                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                                                   <span></span>
                                            </div>
                                        </div>

                                    </form>
                                        
                                    </div>
                                    <!-- END General Data Content -->
                           
                    </div>
                    <!-- END Page Content -->
        @endsection
        