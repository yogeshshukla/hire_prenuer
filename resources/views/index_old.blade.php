<?php use App\Http\Controllers\ServiceController;?>
<?php use App\Http\Controllers\UserController;?>
@include('layouts.outer_header')







<section>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="public/images/s-1.jpg" alt="">
    </div>

    <div class="item">
      <img src="public/images/s-2.jpg" alt="">
    </div>
	<div class="carousel-caption">
        <h2>hirepreneurs</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <h3>Lorem Ipsum is simply dummy text of the printing.</h3>
        <button class="slider-bu s-left">Hire</button>
        <button class="slider-bu s-right">WORK</button>
      </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-menu-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-menu-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</section>



<section class="ListingBox">
  <div class="container">
    <div class="row">
      <h2 class="headding1">Listing Of Different Section</h2>
    </div>
<?php $service = ServiceController::getService(); ?>

	<div class="flexslider" id="box11" style="margin:0px">
			  <ul class="slides">			  
			@foreach($service as $value)
				<li>
					<div class="card box1">
						<img class="card-img-top img-responsive" src="public/assets/img/service/{{$value->image}}" alt="" >
						<h5 class="card-title">{{$value->service}}</h5>
						<div class="card-body">			
						<p class="card-text">{{$value->description}}</p>
			
						</div>
					</div>
				</li>
				@endforeach
			<!-- 	<li>
					<div class="card box1">
						<img class="card-img-top img-responsive" src="public/images/software.jpg" alt="" >
						<h5 class="card-title">Design Art & Multimedia</h5>
						<div class="card-body">			
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			
						</div>
					</div>
				</li>
				<li>
					<div class="card box1">
						<img class="card-img-top img-responsive" src="public/images/art.jpg" alt="" >
						<h5 class="card-title">Software Development</h5>
						<div class="card-body">			
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			
						</div>
					</div>
				</li>
				<li>
					<div class="card box1">
						<img class="card-img-top img-responsive" src="public/images/art.jpg" alt="" >
						<h5 class="card-title">Software Development</h5>
						<div class="card-body">			
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			
						</div>
					</div>
				</li> -->
				</ul>
				<script type="text/javascript">
				$(window).load(function(){
				   $('#box11').flexslider({
					animation: "slide",
					animationLoop: true,
					slideshowSpeed: 100,
					animationSpeed: 600,
					slideshow: false,
					startAt: 0,
					initDelay: 0,
					move: 1,
					controlNav: false,
					directionNav: true,
					itemWidth: 350,
					itemMargin: 10
				  });			  
				});
			</script>
		
		

				</div>

    
  </div>
</section>



<section class="ListingBox1">
  <div class="container">
    <div class="row">
      <h2 class="headding1">
		<span>Profiles of Service Provides</span>
	  </h2>
    </div>
    <?php $user_data = UserController::getUser(); ?>
   @foreach($user_data as $val)
	<div class="col-md-3">
	  <div class="card box2">
		@if($val->image!= "" and file_exists('public/assets/img/users/'.$val->image)) 
		<img class="card-img-top user-profile" src="public/assets/img/users/{{$val->image}}}" alt="" >
		@else
		<img class="card-img-top user-profile" src="public/assets/img/users/default.jpg" alt="" >
		@endif
		<div class="card-body1">		
			<div class="rating-block">
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				&nbsp;
				<strong>2.5/5</strong>
			</div>
		    <p class="username">{{$val->name}}</p>	
			<p class="usertitle">{{$val->name}}</p>	
			<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> {{$val->price}}</p>	
		</div>
	  </div>
	</div>
@endforeach
	 <div class="col-md-3">
	  <div class="card box2">
		<img class="card-img-top user-profile" src="public/images/12.jpg" alt="" >
		<div class="card-body1">		
			<div class="rating-block">
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				&nbsp;
				<strong>4.5/5</strong>
			</div>
		    <p class="username">John Donne</p>	
			<p class="usertitle">Web Developer</p>	
			<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 450</p>	
		</div>
	  </div>
	</div>
<!--
	<div class="col-md-3">
	  <div class="card box2">
		<img class="card-img-top user-profile" src="public/images/13.jpg" alt="" >
		<div class="card-body1">		
			<div class="rating-block">
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				&nbsp;
				<strong>3/5</strong>
			</div>
		    <p class="username">John Donne</p>	
			<p class="usertitle">Android Developer</p>	
			<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 400</p>	
		</div>
	  </div>
	</div> -->

	<div class="col-md-3">
	  <div class="card box2">
		<img class="card-img-top user-profile" src="public/images/14.jpg" alt="" >
		<div class="card-body1">		
			<div class="rating-block">
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				&nbsp;
				<strong>4.5/5</strong>
			</div>
		    <p class="username">John Donne</p>	
			<p class="usertitle">IOS Developer</p>	
			<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 150</p>	
		</div>
	  </div>
	</div>

    
  </div>
</section>



<section class="ListingBox2">
  <div class="container">
    <div class="row">
      <h2 class="headding1">
		<span>Popular Right Now</span>
	  </h2>
    </div>

	<div class="flexslider" id="box12" style="margin:0px">
		<ul class="slides">			  
		<li>
			<div class="card box3">
				<div class="col-md-5">
				  <img class="card-img-top user-profile" src="public/images/12.jpg" alt="" >
				</div>
				<div class="col-md-7">
					<div class="card-body1">						
						<p class="username">John Donne</p>	
						<p class="usertitle">Web Developer</p>	
						<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 450</p>	
						<div class="rating-block">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							&nbsp;
							<strong>4.5/5</strong>
						</div>
					</div>
				</div>
			  </div>
		</li>
		<li>
			<div class="card box3">
				<div class="col-md-5">
				  <img class="card-img-top user-profile" src="public/images/13.jpg" alt="" >
				</div>
				<div class="col-md-7">
					<div class="card-body1">						
						<p class="username">John Donne</p>	
						<p class="usertitle">Web Developer</p>	
						<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 450</p>	
						<div class="rating-block">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							&nbsp;
							<strong>4.5/5</strong>
						</div>
					</div>
				</div>
			  </div>
		</li>
		<li>
			<div class="card box3">
				<div class="col-md-5">
				  <img class="card-img-top user-profile" src="public/images/14.jpg" alt="" >
				</div>
				<div class="col-md-7">
					<div class="card-body1">						
						<p class="username">John Donne</p>	
						<p class="usertitle">Web Developer</p>	
						<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 450</p>	
						<div class="rating-block">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							&nbsp;
							<strong>4.5/5</strong>
						</div>
					</div>
				</div>
			  </div>
		</li>
		<li>
			<div class="card box3">
				<div class="col-md-5">
				  <img class="card-img-top user-profile" src="public/images/12.jpg" alt="" >
				</div>
				<div class="col-md-7">
					<div class="card-body1">						
						<p class="username">John Donne</p>	
						<p class="usertitle">Web Developer</p>	
						<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 450</p>	
						<div class="rating-block">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
							&nbsp;
							<strong>4.5/5</strong>
						</div>
					</div>
				</div>
			  </div>
		</li>
		</ul>
		<script type="text/javascript">
		$(window).load(function(){
			$('#box12').flexslider({
			animation: "slide",
			animationLoop: true,
			slideshowSpeed: 100,
			animationSpeed: 600,
			slideshow: false,
			startAt: 0,
			initDelay: 0,
			move: 1,
			controlNav: false,
			directionNav: true,
			itemWidth: 450,
			itemMargin: 10
			});			  
		});
	</script>
		
		

		</div>

	</div>

    
  </div>
</section>



<section class="ListingBox3">
  <div class="container">
    <div class="row">
      <h2 class="headding1">
		<span>Recommendations</span>
	  </h2>
    </div>

	<div class="flexslider" id="box13" style="margin:0px">
		<ul class="slides">			  
		<li>
			<div class="card box4">
				<center><img class="card-img-top user-profile" src="public/images/13.jpg" alt="" ></center>
				<div class="card-body1">		
					<p class="username">John Donne</p>	
					<p class="usertitle">Android Developer</p>	
					<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 400</p>	
				</div>
			  </div>
		</li>

		<li>
			<div class="card box4">
				<center><img class="card-img-top user-profile" src="public/images/12.jpg" alt="" ></center>
				<div class="card-body1">		
					<p class="username">John Donne</p>	
					<p class="usertitle">Android Developer</p>	
					<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 400</p>	
				</div>
			  </div>
		</li>

		<li>
			<div class="card box4">
				<center><img class="card-img-top user-profile" src="public/images/14.jpg" alt="" ></center>
				<div class="card-body1">		
					<p class="username">John Donne</p>	
					<p class="usertitle">Android Developer</p>	
					<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 400</p>	
				</div>
			  </div>
		</li>

		<li>
			<div class="card box4">
				<center><img class="card-img-top user-profile" src="public/images/12.jpg" alt="" ></center>
				<div class="card-body1">		
					<p class="username">John Donne</p>	
					<p class="usertitle">Android Developer</p>	
					<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 400</p>	
				</div>
			  </div>
		</li>
		</ul>
		<script type="text/javascript">
		$(window).load(function(){
			$('#box13').flexslider({
			animation: "slide",
			animationLoop: true,
			slideshowSpeed: 100,
			animationSpeed: 600,
			slideshow: false,
			startAt: 0,
			initDelay: 0,
			move: 1,
			controlNav: false,
			directionNav: true,
			itemWidth: 300,
			itemMargin: 10
			});			  
		});
	</script>
		
		

		</div>

	</div>

    
  </div>
</section>





<section class="ListingBox4">
	<div class="container">
		<div class="row">
		<h2 class="headding1">Testimonials</h2>
		</div>     
		<div class="flexslider" id="Testm" style="margin:0px">
		   <ul class="slides">	
			 <li>
			    <p>
				<i class="fa fa-quote-left" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. <i class="fa fa-quote-right" aria-hidden="true"></i>
				<span>John Donne</span>
				</p>
			</li>

			 <li>
			    <p>
				<i class="fa fa-quote-left" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. <i class="fa fa-quote-right" aria-hidden="true"></i>
				<span>John Donne</span>
				</p>
			</li>

			 <li>
			    <p>
				<i class="fa fa-quote-left" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. <i class="fa fa-quote-right" aria-hidden="true"></i>
				<span>John Donne</span>
				</p>
			</li>
			</ul>
			<script type="text/javascript">
			$(window).load(function(){
				$('#Testm').flexslider({
				animation: "slide",
				animationLoop: true,
				slideshowSpeed: 7000,
				animationSpeed: 500,
				slideshow: true,
				startAt: 0,
				initDelay: 0,
				move: 1,
				controlNav: false,
				directionNav: false,
				});			  
			});
		</script>
		</div>
	</div>
</section>



<section class="news-lat">
<div class="container">
<div class="row">
<h2>Newsletter Subscription</h2>
</div>
	<div class="row">
		<div class="col-sm-8 col-xs-offset-2">
    	 <div class="well-a">
             <form action="#">
              <div class="input-group">
                 <input class="btn btn-lg" name="email" id="email" type="email" placeholder="Your Email" style="text-align:left;">
                 <button class="btn btn-info btn-lg" type="submit" style="text-align:center;">Submit</button>
              </div>
             </form>
    	 </div>
    	 </div>
	</div>
</div>
</section>


<footer id="divFooter" class="footer1">
         <div class="container">
         <div class="row">
                <div class="col-lg-3 col-md-3">            
                <ul class="list-unstyled clear-margins">                        
                    <li class="widget-container widget_nav_menu">                    
                        <h1 class="title-widget wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Hirepreners</h1>                                
                        <ul class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
 							<li><a href="javascript:void(0)"> About Us</a></li>
                            <li><a href="javascript:void(0)"> Contact Us</a></li>
                            <li><a href="javascript:void(0)"> How It Work</a></li>
                                                      
                        </ul>                    
					</li>                            
                </ul>
                </div>
                <div class="col-lg-3 col-md-3">
                <ul class="list-unstyled clear-margins">                        
                    <li class="widget-container widget_nav_menu">                    
                        <h1 class="title-widget wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">UseFul Link</h1>                                
                        <ul class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <li><a href="javascript:void(0)"> Categories</a></li>
                            <li><a href="javascript:void(0)"> Participant Guidelines </a></li>
                            <li><a href="javascript:void(0)"> Memberships</a></li>
                        </ul>                    
					</li>                            
                </ul> 
                </div>
                <div class="col-lg-3 col-md-3">            
                <ul class="list-unstyled clear-margins">                        
                    <li class="widget-container widget_nav_menu">                    
                        <h1 class="title-widget wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">More</h1>                                
                        <ul class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">                        
 				        <li><a href="javascript:void(0)"> Privacy Policy</a></li>
				        <li><a href="javascript:void(0)"> Terms &amp; Conditions</a></li>
						<li><a href="javascript:void(0)"> Privacy & Cookies</a></li>
                        </ul>                    
					</li>                            
                </ul>  
                </div>
                <div class="col-lg-3 col-md-3">  
                        <ul class="list-unstyled clear-margins">                        
                        	<li class="widget-container widget_recent_news">                    
                                <h1 class="title-widget">Contact Detail </h1>                                
                                <div class="footerp wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">                                 
                                <p><span>Email id:</span> <a href="mailto:info@hirepreners.com">info@hirepreners.com</a></p>
                                <p><span>Phone Numbers : </span>+91 - XXXXXXXXXX </p>
                                </div>
                                <div class="social-icons wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">                                
                                	<ul class="nomargin">                                    
                                    <a href="javascript:void(0)"><i class="fa fa-facebook-square fa-3x social-fb" title="Facebook" id="social"></i></a>
	                                <a href="javascript:void(0)"><i class="fa fa-twitter-square fa-3x social-tw" title="Twitter" id="social"></i></a>
	                                <a href="javascript:void(0)"><i class="fa fa-google-plus-square fa-3x social-gp" title="Google Plus" id="social"></i></a>
	                                <a href="javascript:void(0)"><i class="fa fa-linkedin-square fa-3x social-em" title="Linkedin" id="social"></i></a>                                    
                                    </ul>
                                </div>
                    		</li>
                          </ul>
                       </div>
                    </div>
                </div>
                </footer>
				<div class="footer-w3ls">
					<div class="container">
							� 2018 Hirepreners. All rights reserved 
					</div>
				</div>




</body>
</html>
