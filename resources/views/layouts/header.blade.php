<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>HirePreneurs</title>
<link href="{{url('public/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/css/style.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/css/custom.css')}}" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
<script src="{{url('public/js/jquery.min.js')}}" type="text/javascript" ></script>
<script src="{{url('public/js/bootstrap.js')}}" type="text/javascript" ></script>
<script src="{{url('public/js/jquery.flexslider.js')}}" type="text/javascript" ></script>

<script>
$(document).ready(function(){
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
});
</script>
</head>
<body>


<header>
<div class="container">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{url('/')}}">
      <img src="{{url('public/images/logo.png')}}" alt="">
        </a>
      </div>
      <div id="navbar1" class="navbar-collapse collapse float-right">
        <ul class="nav navbar-nav">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Professionals</a></li>
            <li><a href="#">How it Works</a></li>
            <li><a href="#">About Us</a></li>
              @guest
              <li><a href="{{url('/auth/register/seller')}}">Become a Preneur</a></li>
              @else
              @if(Auth::user()->role=='User')
                <li><a href="{{url('auth/seller_role/'.Auth::user()->id)}}">Become a Preneur</a></li> 
                <!-- if register as user and want to become seller as well -->
              @endif
            @endguest
          </ul>
          </li>
      @guest
				<li><a href="{{url('/auth/register/seller')}}">Become a Preneur</a></li>
			@else
				@if(Auth::user()->role=='User')
					<li><a href="{{url('auth/seller_role/'.Auth::user()->id)}}">Become a Preneur</a></li> 
          <!-- if register as user and want to become seller as well -->
				@endif
				
			@endguest	
			
			@guest
				
				<li><a href="{{url('/auth/register')}}">Sign Up</a></li>
				<li><a href="{{url('/auth/login')}}">Log In</a></li>
            @else
				<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
								<li><a href="{{ url('profile') }}">Profile</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
			<li><a href="{{url('/cart')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
            
                        
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
</header>
