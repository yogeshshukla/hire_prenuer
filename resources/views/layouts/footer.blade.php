
<footer>
<div class="footer-div">
<div class="container">
<div class="row">
<div class="col-sm-4">
<div class="link-menu">
<ul>
<li><a href="#">ABOUT US</a></li>
<li><a href="#">CATEGORIES</a></li>
<li><a href="#">HOW IT WORKS</a></li>
</ul>
</div>
</div>
<div class="col-sm-4">
<div class="link-menu">
<ul>
<li><a href="#">PARTICIPANT GUIDELINES</a></li>
<li><a href="#">MEMBERSHIPS</a></li>
<li><a href="#">FAQ'S</a></li>
</ul>
</div>
</div>
<div class="col-sm-4">
<div class="link-menu">
<ul>
<li><a href="#">CONTACT US</a></li>
<li><a href="#">TERMS AND CONDITIONS</a></li>
<li><a href="#">PRIVACY & COOKIES</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="copy-right">
<div class="container">
<div class="row">
<p>hirepreneurs LLC. All Right Reserved 2018</p>
</div>
</div>
</div>
</footer>

</body>
</html>
