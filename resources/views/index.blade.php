<?php use App\Http\Controllers\ServiceController;?>
<?php use App\Http\Controllers\UserController;?>
@include('layouts.outer_header')




<section class="color-div">
<div class="container">
<div class="row">

	<center>
	<h3 class="homeText1">
Hire professional independent <br/>
consultants from all over the country.
</h3>
	
       @include('includes.search')
	</center>
</div>
</div>
	<img src="public/images/home-banner.jpg" style="width:100%;" >
</section>

<section class="HomeMainBox">
  <div class="container">

  
  
	<div class="col-md-12 p0">
	    <h2 class="headding1" style="border-top:none">Writers</h2>
	<?php $writer_data = UserController::getUserbySkill('Writer'); ?>
   @foreach($writer_data as $val)
	<div class="col-md-3 pleft0">
		  <div class="card box2 ">
		    <div class="homeBoxUser">
			<a href="{{url('/profile/'.$val->user_id)}}">
			@if($val->image!= "" and file_exists('storage/app/'.$val->image)) 
			<img class="card-img-top user-profile" src="{{url('storage/app/'.$val->image)}}" alt="" > 
			@else
			<img class="card-img-top user-profile" src="public/assets/img/users/default.jpg" alt="" >
			@endif
			</a>
			</div>
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i>{{$val->price}}</p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>5</strong>
				</div>
			</div>
		  </div>
		</div>
		
	@endforeach	
		
	</div>
   
	<div class="col-md-12 p0">
	    <h2 class="headding1">Accountants</h2>
	   <div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/software.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>12</strong>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/art.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>8</strong>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/ui.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>13</strong>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/software.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>11</strong>
				</div>
			</div>
		  </div>
		</div>
		
	</div>
     <!---box--->
	 
	 <!---box--->
	<div class="col-md-12 p0">
	    <h2 class="headding1">Lawyers</h2>
	   <div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/software.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>18</strong>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/art.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>19</strong>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/ui.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>20</strong>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="col-md-3 pleft0">
		  <div class="card box2">
			<img class="card-img-top user-profile" src="public/images/software.jpg" alt="" >
			<a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
			<div class="card-body1">	
				<p class="usertitle">Market Visit : SEVILLE</p>			
				<p class="username">New : Triana Market guided visit</p>				
				<p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>	
				<div class="rating-block">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					&nbsp;
					<strong>22</strong>
				</div>
			</div>
		  </div>
		</div>
		
	</div>
     <!---box--->
	
	
  </div>
</section>

@include('layouts.footer')


