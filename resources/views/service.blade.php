@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table>
                    <?php use App\Http\Controllers\ServiceController;?>
                    @foreach($data as $key=>$val)
                        <tr><th>{{$val->cat_name}}</th></tr>
                        <?php $service = ServiceController::getService($val->id); ?>
                         @foreach($service as $key=>$value)
                        <tr><td>{{$value->service}}</td></tr>
                        @endforeach
                        
                    @endforeach
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
