@include('layouts.header')
    

<section class="color-div">
<div class="container">
      <div class="col-sm-3">
        <div class="img-upload-div">
          <div class="upload-img">
          
          @if($data->image != "")
          <img src="{{url('storage/app/'.$data->image)}}" />
          @else
          <img src="{{url('public/assets/img/users/default.jpg')}}" />
          @endif
          </div>
        </div>

        <div class="con">
          <p class="text-center;">
          <button type="submit" class="searchBtn" data-toggle="modal" data-target="#editPopup">Ask a Question </button>
          </p>
        </div>
        <div class="con">
          <strong> Contact</strong>
          <p>{{$data->website}}</p>
          <p>{{$data->email}}</p>
        </div>
      </div>
      <div class="col-sm-9">
        @if(Auth::check())
        @if(Auth::user()->role == 'Seller')
         <a href="update-profile">
         <button type="submit" class="searchBtn float-right" >Edit Profile </button>
       </a>
       @endif
       @endif
         <div class="clearfix"></div>
         
         <div class="userName">  
           <p>{{$data->name}}</p>
           <div class="rating-block1">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            &nbsp;
            <strong></strong>
          </div>
          </div>
          <div class="UserPost">
             <span> {{$data->title}} </span>  
			 <p><span><i class="fa fa-map-marker"></i> New Delhi</span>  </p>
          </div>
         @if($data->role == 'Seller')
          <p class="float-left profileP">
         {{$data->about_me}} 
       
          </p>
          
          <div class="col-md-6 pleft0">
			  <div class="con">
				<strong> Degree</strong>
				<p>{{$data->degrees}}</p>
			  </div>
			  
			   <div class="con">
				<strong> Experience</strong>
				<p>{{$data->experience}}</p>
              </div>  
			   <div class="con">
				<strong> Industries</strong>
				<p>{{$data->industries}}</p>
			  </div>			  
          </div>
          
          <div class="col-md-6 pleft0">
			  <div class="con">
				<strong> Skill</strong>
				<p>{{$data->skills}}</p>
			  </div>		  
			   <div class="con">
				<strong> Awards</strong>
				<p>{{$data->award_certification}}</p>
			  </div>
          </div>
         @endif
         <div class="clearfix"></div>

      </div>
        </div>

		<br/>
</section>

<section>
      <div class="container">
           <div class=" col-md-3">
           <h2 class="cat-headding">Other Offers</h2>
         <ul class="offerList">
            <li> 
             <img src="{{url('public/images/software.jpg')}}">
           <p class="other1">Lorem Ipsum is simply dummy</p>
           <p class="userpriceList"><i class="fa fa-usd" aria-hidden="true"></i> 8.00 </p>  
          </li>
           <li> 
             <img src="{{url('public/images/ui.jpg')}}">
           <p class="other1">Lorem Ipsum is simply dummy</p>
           <p class="userpriceList"><i class="fa fa-usd" aria-hidden="true"></i> 8.00 </p>  
          </li>
           <li> 
             <img src="{{url('public/images/art.jpg')}}">
           <p class="other1">Lorem Ipsum is simply dummy</p>
           <p class="userpriceList"><i class="fa fa-usd" aria-hidden="true"></i> 8.00 </p>  
          </li>
         </ul>
       </div>
       
       <div class="col-md-9">
           @if($data->role == 'Seller')
           <h2 class="cat-headding1">{{$data->project_name}}</h2>
          <p>{{$data->project_description}}  </p>
         <ul class="offerList1">
            <li>
            <h4>What it includes</h4>
          <p>
            {{$data->includes}} 
          </p>
          </li>
          <li>
            <h4>What it does not include</h4>
          <p>
           {{$data->excludes}}
          </p>
          </li>
          <li>
            <h4>Notes</h4>
          <p>
           {{$data->includes}}
          </p>
          </li>
          
          
          
         </ul>
         
         <div class="col-md-6 pleft0">
          <h4>Offer Expires : 15-06-2018</h4>
         </div>
         <div class="col-md-6 pro-price text-right">
             <strong class="reviewPrice"> <i class="fa fa-usd"> </i>  {{$data->price}} </strong>
           <form method="post" action="{{url('/cart')}}">
            <input type="hidden" name="offer_id" value="{{$data->offer_id}}">
           <input type="hidden" name="project_name" value="{{$data->project_name}}">
           <input type="hidden" name="project_description" value="{{$data->project_description}}">
           <input type="hidden" name="price" value="{{$data->price}}">
           <select name="qty">
           <option value="1">1</option>
           </select>
           <button type="submit" class="searchBtn">Buy</button>
           </form>
         
           <div class="rating-block">
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span class="fa fa-star"></span>
            &nbsp;
            <strong>42 Reviews</strong>
          </div>
        
         </div>
         
         <div class="social-icons">
        <ul>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-envelope"></i></a></li>         
          <li><a href="#"><i class="fa fa-code"></i></a></li>
          <li><a href="#"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></li>
          <li class="float-right save11"> Save to Wish List <i class="fa fa-heart-o"></i></li>
        </ul>
         </div>
         @endif
         <div class="comment-box">
            <textarea rows="2" placeholder="Please Enter Your Comment"></textarea>
          <button type="button" class="searchBtn mtop15">Send</button>
         </div>
       
       

          <ul class="reviewList">
        <h4>Reviews</h4>
          <li>
            <img class="review-user" src="{{url('public/images/12.jpg')}}">
            <h5>User Name </h5>
            <h6>December 18, 2017</h6>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
</p>
          </li>
          
          <li>
            <img class="review-user" src="{{url('public/images/13.jpg')}}">
            <h5>User Name </h5>
            <h6>December 18, 2017</h6>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
</p>
          </li>
          
          <li>
            <img class="review-user" src="{{url('public/images/14.jpg')}}">
            <h5>User Name </h5>
            <h6>December 18, 2017</h6>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
</p>
          </li>
          
          
           <li>
          <div class="col-md-6 p0"><span class="allView">Read all 42 review</span></div>
          <div class="col-md-6">
              <div class="rating-block float-right">
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span>
              &nbsp;
              <strong>42 Reviews</strong>
              </div>
          </div>
          </li>

         </ul>
         
       </div>
       
      
       
    </div>
</section>



<section class="similer-offer">
  <div class="container">
  <h2 class="cat-headding">Similar Deals</h2>
      <div class="flexslider" id="HomeSlide" style="margin:0px">
      <ul class="slides">       
      <li>
        <div class="card box2">
      <img class="card-img-top user-profile" src="{{url('public/images/ui.jpg')}}" alt="" >
      <a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
      <div class="card-body1">  
        <p class="usertitle">Market Visit : SEVILLE</p>     
        <p class="username">New : Triana Market guided visit</p>        
        <p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>  
        <div class="rating-block">
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <span class="fa fa-star"></span>
          &nbsp;
          <strong>2.5/5</strong>
        </div>
      </div>
      </div>
      </li>
      <li>
        <div class="card box2">
      <img class="card-img-top user-profile" src="{{url('public/images/ui.jpg')}}" alt="" >
      <a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
      <div class="card-body1">  
        <p class="usertitle">Market Visit : SEVILLE</p>     
        <p class="username">New : Triana Market guided visit</p>        
        <p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>  
        <div class="rating-block">
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <span class="fa fa-star"></span>
          &nbsp;
          <strong>2.5/5</strong>
        </div>
      </div>
      </div>
      </li>
      <li>
        <div class="card box2">
      <img class="card-img-top user-profile" src="{{url('public/images/ui.jpg')}}" alt="" >
      <a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
      <div class="card-body1">  
        <p class="usertitle">Market Visit : SEVILLE</p>     
        <p class="username">New : Triana Market guided visit</p>        
        <p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>  
        <div class="rating-block">
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <span class="fa fa-star"></span>
          &nbsp;
          <strong>2.5/5</strong>
        </div>
      </div>
      </div>
      </li>
      <li>
        <div class="card box2">
      <img class="card-img-top user-profile" src="{{url('public/images/ui.jpg')}}" alt="" >
      <a href="#" class="heartBtn"><i class="fa fa-heart"></i></a>
      <div class="card-body1">  
        <p class="usertitle">Market Visit : SEVILLE</p>     
        <p class="username">New : Triana Market guided visit</p>        
        <p class="userprice"><i class="fa fa-usd" aria-hidden="true"></i> 11 <span>Per Person</span></p>  
        <div class="rating-block">
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <span class="fa fa-star"></span>
          &nbsp;
          <strong>2.5/5</strong>
        </div>
      </div>
      </div>
      </li>
      </ul>
      <script type="text/javascript">
        $(window).load(function(){
           $('#HomeSlide').flexslider({
          animation: "slide",
        animationLoop: true,
        slideshowSpeed: 100,
        animationSpeed: 600,
        slideshow: false,
        startAt: 0,
        initDelay: 0,
        move: 1,
        controlNav: false,
        directionNav: true,
        itemWidth: 290,
        itemMargin: 10
        });  
        });
      </script>
        </div>
    </div>
</section>


<div id="editPopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Contact Lizet Zayas</h4>
      </div>
      <div class="modal-body">
        <textarea rows="4" class="popupMess">
      
    </textarea> 
    <button type="button" class="searchBtn float-right">Send</button>
    <button type="button" class="backBtn1 float-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
@include('layouts.footer')